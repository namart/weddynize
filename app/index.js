/**
 * The app uses the MVC Pattern. This file loads all controllers and models.
 */

var fs = require('fs'),
    util = require('util');

module.exports = function(app) {

  // Bootstrap models
  var models_path = __dirname + '/models';
  require(models_path + '/' + 'models'); 

  // Bootstrap controllers
  var controllers_path = __dirname + '/controllers';
  var controller_files = fs.readdirSync(controllers_path);
  controller_files.forEach(function(file){
    require(controllers_path+'/'+file)(app);
  });

};
