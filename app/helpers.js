var List = require("./models/ListModel");
var i18n = require('i18next');

var helper = function(app) {

  // dynamic View Helper
  app.dynamicHelpers({

    user: function(req, res){
      return req.user;
    },

    setLocals: function(req, res) {
      if(req.user) {
        req.i18n.setLng(req.user.lang);
      }
    },

    flash: function(req, res){
      return req.flash();
    },
  });

  // View Helper
  app.helpers({

    getSiteUrl: function() {
      return app.config.http.siteUrl;
    },

    error_label: function(errors, key) {
      if(errors != null) {
        if(errors.errors[key] != undefined) {
          return '<label class="errormessage" for="' + key + '"><span>' + errors.errors[key].message + '</span></label>';
        }
      }
    },

    error_class: function(errors, key) {
      if(errors != null) {
        if(errors.errors[key] != undefined) {
          return ' error';
        }
      }
    },

    user_avatar: function(req, res) {
      return "TEST";
    },

    from_now: function(date, key) {
      mil = new Date() - date;
      sec = parseInt(mil/1000);
      min = parseInt(sec/60);
      hou = parseInt(min/60);
      day = parseInt(hou/24);

      returnString = "";

      // less then 1 day
      if(day == 0) {

        //nearly day
        if(hou > 20) {
          returnString = i18n.t("time.one-day");
        }

        //nearly day
        if(hou < 20 && hou > 0) {
          return hou + returnString = i18n.t("time.days");
        }

        // less then 1 hou
        if(hou == 0) {

          //1/2 hour
          if(min < 45 && min > 15) {
            returnString = i18n.t("time.half-hour");
          }

          //nearly hour
          if(min > 45) {
            returnString = i18n.t("time.one-hour");
          }

          //nearly hour
          if(min > 0 && min < 15) {
            returnString = i18n.t("time.several-minutes");
          }

          // less then 1 min
          if(min == 0) {
            returnString = i18n.t("time.less-then") + returnString = i18n.t("time.one-minute");
          }
        }

      // one or more days
      } else {
        if(day == 1) {
          returnString = i18n.t("time.one-day");
        }

        if(day > 1) {
          return day + returnString = i18n.t("time.days");
        }
      }

      return i18n.t(key, {time: returnString});

    },

    lists_as_string: function(lists) {
      returnString = "";
      var i = 0;
      for(i==0; i<lists.length; i++) {
        returnString += lists[i].name;
        if(i < lists.length-1) {
          returnString += ', ';
        }
      }
      return returnString;
    },

  });

  // App Helper
  return {};
};

module.exports = helper;
