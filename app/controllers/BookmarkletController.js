var User = require("../models/UserModel");
var Nize = require("../models/NizeModel");
var List = require("../models/ListModel");
var request = require('request');
var fs = require('fs');
var easyimg = require('easyimage');
var cleanCSS = require('clean-css');
var async = require('async');
var knox = require('knox');

var client = knox.createClient({
  key:  process.env.AWS_ACCESS_KEY_ID,
  secret:  process.env.AWS_SECRET_ACCESS_KEY,
  bucket:  process.env.S3_BUCKET_NAME
});

module.exports = function(app) {

  app.param('userId', function (req, res, next, id) {
    User.findById(id, function(err, user) {
      if (err) return next(err);
      if (!user) return next(new Error('Failed to load User ' + id));
      req.foundUser = user;
      next();
    });
  });

  /**
   * Bookmarklet overview page
   */
  app.get('/bookmarklet', function(req, res) {
    // TODO check for browser and return specific bookmarklet instruction page
    res.render('pages/bookmarklet', {
      title: 'Add bookmarklet to your browser',
      browser: "chrome",
      user: req.user,
    });
  });

  /**
   * Get Bookmarklet
   */
  app.get('/bookmarklet/:userId.js', function(req, res) {
    var user = req.foundUser;

    List.getAllFromUser(user._id, function(err, lists) {

      if(app.config.env === "development" || true) {
        fs.readFile(__dirname + '/../views/bookmarklet/bookmarklet.css', 'utf8', function(err, data) {
          var minimized = cleanCSS.process(data);
          res.contentType('js');
          res.render('bookmarklet/bookmarklet', {
            userId: user._id,
            lists: lists,
            css: minimized,
            ratings: app.settings.ratings,
            layout: false
          });
        });

      // this code propably needs to be changed for production enviroment
      // because the bookmarkelt should not be rendered -> should be a static file on asset server
      } else {
        res.contentType('js');
        res.render('bookmarklet/bookmarklet', {
          userId: user._id,
          lists: lists,
          css: "change this to production css code",
          ratings: app.settings.ratings,
          layout: false
        });
      }
    });
  });

  /**
   * adds a new list
   */
  app.get('/bookmarklet/:userId/newlist', function(req, res) {
    var user = req.foundUser;

    List.add(user._id, { name: req.query.name }, function(err, list) {
      console.log('list added?: ', list);
      res.json(list);
    });

  });

  /**
   * Creates a Nize from the bookmarklet
   * req.query: {
       lists: listIds,
       title: title,
       description: description,
       image: image source,
       link: from link,
       referrer: refere link
      }
   */
  app.get('/bookmarklet/:userId', function(req, res) {

    var nize = {
      title: req.query.title,
      description: req.query.description,
      lists: req.query.lists,
      link: req.query.link,
      referer: req.query.referrer,
      rating: req.query.rating
    };

    var formats = ['jpg', 'gif', 'png'];

    var mimeType = {
      'image/jpeg': 'jpg',
      'image/gif': 'gif',
      'image/png': 'png'
    };

    var format = req.query.image.split('.').pop();

    var isProduction = process.env.NODE_ENV === 'production';


    Nize.add(req.params.userId, nize, function(err, nize) {

      var extention = '';
      if(formats.indexOf(format) > -1) {
        extention += '.' + format;
      }

      /**
       * returns a path to file
       * @params {String} type - o: original, b: big(resized), s: small(thumb)
       * @params {Boolean} filename
       */
      var imageUrl = function(type, filename) {
        if(filename) {
          return '/' + nize._id + '_' + type + extention;
        }
        return app.set('tmpDir') + '/' + nize._id + '_' + type + extention;
      };

      var imageUrlS3 = function(type) {
        return app.config.http.s3 + '/' + nize._id + '_' + type + extention;
      };

      var enviroment = process.env.NODE_ENV;

      var originalLocation = imageUrl('o');
      var hasExtention = true;
      var writeStream = fs.createWriteStream(originalLocation);
      var r = request(req.query.image);
      r.pipe(writeStream);

      r.on('end', function () {

        // if image was without format then add it now
        if(extention === '') {
          hasExtention = false;
          extention += '.' + mimeType[r.response.headers['content-type']];
          format = extention;
        }

        // maybe change to parallel
        async.series([

          // create thumb
          function(callback) {
            easyimg.thumbnail({
              src: originalLocation,
              dst: imageUrl('s'),
              width: '200',
              gravity:  'North'
            }, function(err, image) {
              callback(err);
            });
          },

          // create big image
          function(callback) {

            easyimg.info(imageUrl('o'), function(err, img) {

              // resize to fit
              if(img.width > 600) {
                easyimg.resize({
                  src: originalLocation,
                  dst: imageUrl('b'),
                  width: 600
                }, function(err, image) {
                  if (err) throw err;
                  callback(err, true);
                });

              // take original
              } else {
                if(!hasExtention) {
                  fs.rename(originalLocation, imageUrl('o'), function (err) {
                    callback(err);
                  });
                } else {
                  callback(err);
                }
              }
            });
          }

        ], function(err, results) {

          if(err) throw err;

          // delete original
          var type = 'o';
          if(results[1]) {
            type = 'b';
            fs.unlink(originalLocation, function (err) {
              if(err) throw err;
            });
          }

          var updateNize = function() {
            var nizeAdds = {
              original: isProduction ? imageUrlS3(type) : '/' + imageUrl(type),
              thumb: isProduction ? imageUrlS3('s') : '/' + imageUrl('s'),
              format: r.response.headers['content-type'],
              size: r.response.headers['content-length']
              // size needs to be the resized image
            };

            Nize.updateNize(nize._id, nizeAdds, function(err) {
              if(err) throw err;
              res.json(nize);
            });
          };

          if(isProduction || true) {

            async.series([

              // upload thumb to S3
              function(callback) {
                console.log(imageUrl('s'));
                console.log(imageUrl('s', true));
                client.putFile(imageUrl('s'), imageUrl('s', true), function(err, res){
                  if(err) throw err;
                  // delete thumb localy
                  fs.unlink(imageUrl('s'), function (err) {
                    if(err) throw err;
                    callback();
                  });
                });
              },

              // upload big image to S3
              function(callback) {
                client.putFile(imageUrl(type), imageUrl(type, true), function(err, res){
                  if(err) throw err;
                  // delete thumb localy
                  fs.unlink(imageUrl(type), function (err) {
                    if(err) throw err;
                    callback();
                  });
                });
              },

            ], function(err, results) {
              if(err) throw err;
              updateNize();
            });

          } else {
            updateNize();
          }

        });
      });

      writeStream.on('error', function (err) {
        console.log(err);
      });

    });

  });

};
