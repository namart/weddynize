var List = require("../models/ListModel");

module.exports = function(app){

  /**
   * returns all lists
   */
  app.get('/list', function(req, res) {
    if(req.user) {
      List.getAllFromUser(req.user._id, function(err, lists) {
        if(err) throw err;

        // check if we have a XMLhttpRequest(Ajax)
        if(req.xhr) {
          res.json(lists);
        } else {
          res.render('lists/index', {
            title: 'List of Lists',
            lists: lists
          });
        }
      });
    } else {
      List.getAllFromAdmins(function(err, lists) {
        if(err) throw err;

        // check if we have a XMLhttpRequest(Ajax)
        if(req.xhr) {
          res.json(lists);
        } else {
          res.render('lists/index', {
            title: 'List of Lists',
            lists: lists
          });
        }
      });
    }
  });

  /**
   * New List
   */
  app.get('/list/new', app.ensureAuthenticated, function(req, res) {
    res.render('lists/new', {
      title: 'New List'
    });
  });

  /**
   * Creates a new List
   */
  app.post('/list', app.ensureAuthenticated, function(req, res) {
    var list = {
      name: req.body.name
    };
    List.add(req.user._id, list, function(err, list) {
      // check if we have a XMLhttpRequest(Ajax)
      if(req.xhr) {
        res.json(list);
      } else {
        res.redirect('/list/'+list._id);
      }
    });
  });

  /**
   * Delete a List
   */
  app.delete('/list/:id', app.ensureAuthenticated, function(req, res) {
    List.delete(req.params.id, function(err){
      if (err) return next(err);
      res.json(req.params.id);
    });
  });

};
