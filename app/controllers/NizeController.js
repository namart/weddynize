var Nize = require("../models/NizeModel");
var List = require("../models/ListModel");
var image = require("../../lib/image");
var async = require('async');

module.exports = function(app){

  /**
   * Search for nizes by query
   */
  app.get('/nize/search.:format', function(req, res, next) {
    switch (req.params.format) {
      case 'json':
        var userId = req.user ? req.user._id : null;
        Nize.get(userId, req.query, function(err, nizes) {
          if (err) throw err;

          res.json(nizes);
        });
        break;
      default:
        res.send(400);
        break;
    }
  });


  /**
   * Creates a new Nize
   */
  app.post('/nize', app.ensureAuthenticated, function(req, res) {
    var nize_inputs = {
      title: req.body.title,
      description: req.body.description,
      lists: req.body.lists,
      rating: req.body.rating,
      link: req.body.link,
      original: req.body.original,
      thumb: req.body.thumb,
      size: req.body.size,
      format: req.body.format,
    };

    Nize.add(req.user._id, nize_inputs, function(err, nize) {

      // check if we have a XMLhttpRequest(Ajax)
      if(req.xhr) {
        res.json(nize);
      } else {

        if(err) {
          List.getAllFromUser(req.user._id, function(list_err, lists) {
            res.render('pages/add', {
              title: 'New Nize',
              lists: lists,
              nize: nize_inputs,
              errors: err,
              message: req.flash('error', 'There was an error. Pleas fix your input.')
            });
          });
        } else {

          // when we successfully saved the nize we need to send it to the asset server
          var isProduction = process.env.NODE_ENV === 'production';

          if(isProduction || true) {

            /*
             * fields we will update druing transportation
             */
            var nizeUpdates = {};

            /*
             * batch transport several images
             */
            async.parallel([
              /*
               * transport the original image to the asset server.
               */
              function(callback){
                image.transport(nize.original, '/' + nize._id + "_o." + nize.format, function(err, information){
                  if (err) throw err;
                  nizeUpdates.original = app.config.http.s3 + information.name;
                  callback(err);
                });
              },

              /*
               * transport the thumbnail image to the asset server.
               */
               function(callback){
                  image.transport(nize.thumb, '/' + nize._id + "_s." + nize.format, function(err, information){
                    if (err) throw err;
                    nizeUpdates.thumb = app.config.http.s3 + information.name;
                    callback(err);
                  });
               }

              /*
               * call callback after completion.
               */
            ], function(err, results) {
              /*
               * update original link.
               */
              Nize.updateNize(nize._id, nizeUpdates, function(err){
                if (err) throw err;

                req.flash('success', 'Nize was uplodaded sucessfully.')
                res.redirect('home');
              });
            });
          }
        }
      }
    });
  });

  /**
   * Delete a Nize
   */
  app.delete('/nize/:id', app.ensureAuthenticated, function(req, res) {

    Nize.getNizeById(req.params.id, function(err, nize) {
      if (err) return next(err);
      if (!nize) return next(new Error('Failed to load nize ' + req.params.id));

      if(parseInt(req.user._id) == parseInt(nize._creator)) {
        Nize.deleteNize(req.params.id, function(err, nize) {
          res.json(req.params.id);
        });
      } else {
        res.send(401);
      }
    });
  });

  /**
   * Edit Nize
   */
  app.put('/nize/:id', app.ensureAuthenticated, function(req, res) {
    var attr = {
      title: req.body.title,
      description: req.body.description,
      lists: req.body.lists,
      rating: req.body.rating,
    };

    Nize.updateNize(req.params.id, attr, function(err){
      if(err) {
        res.send(500);
      } else {
        res.json(req.body._id);
      }
    });
  });

  /**
   * Uploads Nize image
   */
  app.post('/nize/upload', app.ensureAuthenticated, function(req, res) {

    /*
     * get the one file uploaded
     */
    if(!req.files) return res.json({type: 'no file'});
    for (var file in req.files) break;
    var file = req.files[file];

    /*
     * use our universal image creator to create image + thumb.
     */
    image.create(file, app.set('tmpDir'), 200, function(err, fileinfos){
      if (err) throw err;

      /*
       * responde the file information
       */
      res.send(fileinfos);
    });

  });

  /**
   * Returns "Add Nize" page
   */
  app.get('/nize/add', app.ensureAuthenticated, function(req, res) {
    getLists(req.user, function(err, lists) {
      res.render('pages/add', {
        title: 'Add Nize',
        lists: lists,
        user: req.user,
        ratings: app.settings.ratings
      });
    });
  });

  /**
   * View Nize
   */
  app.get('/nize/:id', function(req, res, next) {
    if(req.params.id === 'undefined' || req.params.id === false) return next();

    Nize.getNizeById(req.params.id, function(err, nize) {
      if (err) return next(err);
      if (!nize) return next(new Error('Failed to load nize ' + id));

      Nize.get(req.user._id, {
        list: nize.lists,
        limit: 6,
        open: "false",
        exclude: [req.params.id],
      }, function(err, result) {
        if(result == undefined) {
          result = [];
        }

        List.getAllFromUser(nize._creator, function(err, allLists){

          lists = [];
          var i = 0;
          for(i==0; i<allLists.length; i++) {
            if(nize.lists.indexOf(allLists[i]._id) != -1) {
              lists.push(allLists[i]);
            }
          }

          res.render('nizes/show', {
            title: 'View Nize',
            nize: nize,
            siblings: result,
            lists: lists,
          });
        });
      });
    });
  });



  /**
   * Helper Functions
   */

  // Returns all UserLists
  var getLists = function(user, callback) {
    List.getAllFromUser(user._id, callback);
  };

};
