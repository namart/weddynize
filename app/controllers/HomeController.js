var List = require("../models/ListModel");

module.exports = function(app){


  /**
   * Show home site
   */
  app.get('/', home);
  app.get('/all', home);

};

/**
 * home page route function
 */
var home = function (req, res) {
  getLists(req.user, function(err, lists) {
    res.render('pages/home', {
      title: req.i18n.t('Welcome to Weddynize'),
      lists: lists,
      ratings: req.app.settings.ratings,
      layout: "layouts/home"
    });
  });
};


/**
 * Returns lists
 */
var getLists = function(user, callback) {
  if(user) {
    List.getAll(user._id, callback);
  } else {
    List.getAllFromAdmins(callback);
  }
};
