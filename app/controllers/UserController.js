var User = require("../models/UserModel");
var fs = require('fs');
var util = require('util');
var easyimg = require('easyimage');
var http = require('http');
var request = require('request');
var image = require("../../lib/image");
var async = require('async');

module.exports = function(app) {

  app.param('userId', function (req, res, next, id) {
    User.findById(id, function(err, user) {
      if (err) return next(err);
      if (!user) return next(new Error('Failed to load User ' + id));
      req.foundUser = user;
      next();
    });
  });

  /**
   * Profile View
   */
  app.get('/profile', app.ensureAuthenticated, function(req, res) {
    res.render('users/profile', {
      title : 'User Profile',
      user : req.user
    });
  });

  /**
   * Show formular for account changes
   */
  app.get('/settings', app.ensureAuthenticated, function(req, res) {
    res.render('users/settings', {
      title : 'Account Settings',
      user : req.user
    });
  });

  /**
   * Saves new account changes
   */
  app.post('/settings', app.ensureAuthenticated, function(req, res) {
    // Change Email
    if(req.body.func_email) {
      User.update(req.user._id, {
        email: req.body.email
      }, function(err) {

        req.flash('success', 'Your email has been changed!');
        res.redirect('back');
      });
    }

    // Change Language
    if(req.body.func_lang) {
      User.update(req.user._id, {
        lang: req.body.lang
      }, function(err) {

        req.flash('success', 'Your language settings have been changed!');
        res.redirect('back');
      });
    }

    // Change Password
    if(req.body.func_password) {
      User.update(req.user._id, {
        old_password: req.body.old_password,
        new_password: req.body.new_password,
        verify_password: req.body.verify_password,
      }, function(err) {

        req.flash('success', 'Your password has been changed!');
        res.redirect('back');
      });
    }
  });

  /**
   * Uploads Avatar image
   */
  app.post('/settings/upload', app.ensureAuthenticated, function(req, res) {

    //if no file is there
    if(!req.files) return res.json({type: 'no file'});

    // take only the first because we dont support more
    var userId = req.user._id;
    for (var file in req.files) break;
    file = req.files[file];

    /*
     * use our universal image creator to create image + thumb.
     */
    image.create(file, app.set('tmpDir'), 40, function(err, fileinfos){
      if (err) throw err;

      /*
       * transport straight to asset server
       */
      var isProduction = process.env.NODE_ENV === 'production';

      if(isProduction || true) {

        /*
         * fields we will update druing transportation
         */
        var userUpdates = {};

        /*
         * batch transport several images
         */
        async.parallel([
          /*
           * transport the original image to the asset server.
           */
          function(callback){
            image.transport(fileinfos.original, '/' + userId + "_o." + fileinfos.format, function(err, information){
              if (err) throw err;
              userUpdates.original = app.config.http.s3 + information.name;
              callback(err);
            });
          },

          /*
           * transport the thumbnail image to the asset server.
           */
           function(callback){
              image.transport(fileinfos.thumb, '/' + userId + "_s." + fileinfos.format, function(err, information){
                if (err) throw err;
                userUpdates.avatar = app.config.http.s3 + information.name;
                callback(err);
              });
           }

          /*
           * call callback after completion.
           */
        ], function(err, results) {
          /*
           * update original link.
           */
          User.update(userId, userUpdates, function(err) {
            // say GC to remove file
            file = null;

            /*
             * responde the file information
             */
            return res.json(userUpdates);
          });
        });
      }
    });
  });

  /*
   * update user-image from facebook
   */
  app.get('/user/updatefromfacebook', app.ensureAuthenticated, function(req, res) {

    var fid = req.user.facebookId;
    var userId = req.user._id;

    if(fid) {

      // creates dynamically file location
      var imageUrl = function(type) {
        return app.set('tmpDir') + '/' + userId + '_' + type + '.jpg';
      };

      // get user-foto from facebook
      request('http://graph.facebook.com/' + fid + '/picture?type=large', function(error){

        if (error) throw err;

        /*
         * transport straight to asset server
         */
        var isProduction = process.env.NODE_ENV === 'production';

        if(isProduction || true) {

          /*
           * fields we will update druing transportation
           */
          var userUpdates = {};

          /*
           * batch transport several images
           */
          async.parallel([
            /*
             * transport the original image to the asset server.
             */
            function(callback){
              image.transport(imageUrl('o'), '/' + userId + "_o.jpg", function(err, information){
                if (err) throw err;
                userUpdates.original = app.config.http.s3 + information.name;
                callback(err);
              });
            },

            /*
             * transport the thumbnail image to the asset server.
             */
             function(callback){
                image.transport(imageUrl('s'), '/' + userId + "_s.jpg", function(err, information){
                  if (err) throw err;
                  userUpdates.avatar = app.config.http.s3 + information.name;
                  callback(err);
                });
             }

            /*
             * call callback after completion.
             */
          ], function(err, results) {
            /*
             * update original link.
             */
            User.update(userId, userUpdates, function(err) {
              // say GC to remove file
              file = null;

              /*
               * responde the file information
               */
              return res.json(userUpdates);
            });
          });
        }
      }).pipe(fs.createWriteStream(imageUrl('o')));
    } else {
      res.redirect('back');
    }

  });


  /**
   * Creates a new user in the DB
   */
  app.post('/user', function(req, res) {
    User.register(req.body.username, req.body.email, req.body.password, function(err, user) {
      if(err) {
        res.render(
          'forms/register',
          {
            user: req.user,
            errors: err,
            message: req.flash('error', 'There was an error. Pleas fix your input.')
            }
          );
      } else {
        req.flash('info', 'Your account has been created.');
        res.redirect('/login');
      }
    });
  });

  /**
   * This Function outputs the login form
   */
  app.get('/login', function(req, res) {
    if(!req.user) {
      res.render('forms/login', { user: req.user});
    } else {
      res.redirect('back');
    }
  });

  /**
   * Logins a user
   */
  app.post('/login', app.passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  }));

  /**
   * Logouts a user
   */
  app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/login');
  });

  /**
   * This Function outputs the registration form
   */
  app.get('/register', function(req, res) {
    res.render('forms/register', { user: req.user, message: req.flash('error') });
  });

  /**
   * facebook Login
   */
  app.get('/auth/facebook', app.passport.authenticate('facebook', { scope: ['email'] }), function(req, res){
    // The request will be redirected to Facebook for authentication, so
    // this function will not be called.
  });
  app.get('/auth/facebook/callback', app.passport.authenticate('facebook', { failureRedirect: '/login' }), function(req, res) {
    // Successful authentication, redirect home.
    req.flash('success', 'You logged in successfully.')
    res.redirect('home');
  });

};
