var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    timestamps = require('./mongoose-plugins/timestamps');

/**
 * Nize schema
 */
var nizeSchema = new Schema({
  _creator : { type: Schema.ObjectId, ref: 'User', index: true },
  title: {type: String, min: 4, validate: [validate_nize_title, 'validation_error_nize_title']},
  description: String,
  link: String,
  referer: String,
  format: String,
  size: Number,
  original: String,
  thumb: String,
  open: { type: Boolean, default: false },
  lists: [{ type: Schema.ObjectId }],
  rating: { type: Number, min: 0, max: 4, default: 0 },
  _keywords: [{ type: String, index: true }]
});

nizeSchema.plugin(timestamps);

/**
 * List schema embedded in User schema
 */
var listSchema = new Schema({
  name: { type: String, min: 3, max: 20 },
  open: { type: Boolean, default: false },
});

listSchema.plugin(timestamps);

/**
 * User schema
 */
var userSchema = new Schema({
  username: { type: String, unique: true, validate: [validate_username, 'validation_error_username'] },
  email: { type: String, unique: true, validate: [validate_email, 'validation_error_email'] },
  hash: String,
  facebookId: { type: String },
  ip: String,
  avatar: {type: String, default: "/images/avatar.png", },
  original: {type: String, default: "/images/avatar.png", },
  referer: String,
  lastLogin: Date,
  role: { type: String, default: 'user', index: true },
  lang: { type: String, default: 'en', min: 2, max: 2 },
  lists: [listSchema]
});

userSchema.plugin(timestamps);

/**
 * This Function creates keywords out of the content for full text search
 */
nizeSchema.pre('save', function(next) {
  this._keywords = extractKeywords(this.title, this.description);
  next();
});

function extractKeywords() {

  var k = [];
  for(var i=0, len=arguments.length; i<len; i++) {
    var j = arguments[i].split(/\s+/)
    .filter(function(v) { return v.length > 2; })
    .filter(function(v, i, a) { return a.lastIndexOf(v) === i; })
    .map(function(v) { return v.toLowerCase() });
    k = k.concat(j);
  }

  return k;
}


/**
 * Validation callbacks
 * Definition of all validation callbacks, attributes can be checked against. Mongoosejs default's are min, max, enum.
 */

/**
 * Username validation
 */
function validate_username(v) {
  // Basicly check for length, but we can do more validations later in here
  return v.length > 2;
}

/**
 * Nize title validation
 */
function validate_nize_title(v) {
  // Basicly check for length, but we can do more validations later in here
  return v.length > 4;
}

/**
 * Email validation
 */
function validate_email(v) {
  return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(v);
}

/**
 * Numeric validation
 */
function validate_numeric(v) {
  return /^[0-9]$/.test(v);
}

/**
 * Weak password validation
 */
function validate_weak_password(v) {
  return /(?=.{6,}).*/.test(v);
}

/**
 * Medium password validation
 */
function validate_medium_password(v) {
  return /^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$/.test(v);
}

/**
 * Strong password validation
 */
function validate_strong_password(v) {
  return /^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$/.test(v);
}


var Nize = mongoose.model('Nize', nizeSchema);
var List = mongoose.model('List', listSchema);
var User = mongoose.model('User', userSchema);

exports.Nize = Nize;
exports.List = List;
exports.User = User;
