var models = require('./models'),
    List = require('./ListModel'),
    bcrypt = require('bcrypt'),
    _ = require('underscore');

var Nize = function() {

  /**
   * adds a nize to specific user and list
   * @param {String} userId
   * @param {Object} nize
   * @param {Function} callback
   */
  var add = function(userId, nize, callback) {

    var newNize = new models.Nize(nize);
    newNize._creator = userId;
    if(nize.lists && nize.lists.length > 0) {

      newNize.save(callback);

      /*
       * For adding nizeIds to lists in user collection
      models.User.findOne({_id: userId}, function(err, user) {
        if(err) throw err;

        for(var j=0,lenj=nize.lists.length; j<lenj; i++) {
          var nizeList = nize.lists[j];
          var listExists = false;
          for(var i=0,len=user.lists.length; i<len; i++) {
            var userList = user.lists[i];
            if(nizeList.name == userList.name) {
              //list already exists
              listExists = true;
              break;
            }
          }
          if(listExists) {
            // save nize in existing list
            List.addNizeToList(userList._id, newNize, function(err) {
              newNize.save(callback);
            });
          }
        }
      });
      */
    } else {
      List.add(userId, null, function(err, list) {
        newNize.lists.push(list._id);
        newNize.save(callback);
      });
    }
  };

  /**
   * returns all nizes
   * @param {Function} callback
   */
  var getAll = function(callback) {
    models.Nize.find(callback);
  };

  /**
   * returns all nizes from specific user
   * @param {String} userId
   * @param {Function} callback
   */
  var getAllFromUser = function(userId, callback) {
    models.Nize.find({'_creator': userId}, callback);
  };

  /**
   * returns all nizes filterd by params
   * @param {Object} userId
   * @param {Object} params
   * @param {Function} callback
   */
  var get = function(userId, params, callback) {

    var skip = params.offset || 0;
    var limit = params.limit || 0;
    var query = models.Nize;
    var sort = 'updatedAt';
    var asc = -1;

    if(params.list && params.list !== '0') {
      query = query.where('lists', params.list);
    }

    if(params.rating && params.rating !== '0') {
      query = query.where('rating', params.rating);
    }

    if(params.key) {
      var j = params.key.toLowerCase().split(/\s+/)
        .map(function(v) {return new RegExp("^" + v)});
      query = query.where('_keywords').all(j);
    }

    if(params.exclude) {
      query = query.where('_id').nin(params.exclude);
    }

    if(params.sort) {
      sort = params.sort;
    }

    if(params.asc) {
      asc = params.asc;
    }

    if(userId && params.open === 'false') {
      query = query.where('_creator', userId);
    } else {
      query = query.where('open', true);
    }

    query.skip(skip)
    .sort(sort, asc)
    .limit(limit)
    .run(callback);

  };


  /**
   * get nize by id
   * @param {String} nizeId
   * @param {String} callback
   */
  var getNizeById = function(nizeId, callback) {
    models.Nize.findOne({_id: nizeId}, callback);
  };


  /**
   * updates a nize
   * @param {String} nizeId
   * @param {Object} nize
   * @param {Functon} callback
   */
  var updateNize = function(nizeId, nize, callback) {
    models.Nize.update({_id: nizeId}, nize, callback);
  };


  var deleteNize = function(nizeId, callback) {
    models.Nize.findOne({_id: nizeId}, function(err, nize){
      nize.remove(callback);
    });
  };


  /**
   * return Object which is accessable to the outside
   */
  return {
    add: add,
    getAllFromUser: getAllFromUser,
    getNizeById: getNizeById,
    getAll: getAll,
    get: get,
    updateNize: updateNize,
    deleteNize: deleteNize,
  };

}();

module.exports = Nize;
