var models = require('./models'),
    bcrypt = require('bcrypt'),
    i18n = require("i18next"),
    _ = require('underscore');

var User = function() {

  /**
   * Creates password hash
   * @param {String} cleartext
   * @param {Function} callback
   */
  var _password = function(cleartext, callback) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(cleartext, salt, function(err, hash) {
        return callback(hash);
      });
    });
  };

  /**
   * Register a user
   * @param {String} username
   * @param {String} email
   * @param {String} password
   * @param {Function} callback
   */
  var register = function(username, email, password, role, callback) {
    callback || (callback=arguments[arguments.length-1]);
    (role === 'admin') || (role = 'user');
    var user = new models.User();
    user.username = username;
    user.email = email;
    user.role = role;
    user.lang = i18n.lng();
    _password(password,function(hash){
      user.hash = hash;
      user.save(callback);
    });
  };

  /*
   * Authenticate a user
   * @param {String} name
   * @param {String} password
   * @param {Function} callback
   */
  var authenticate = function(name, password, callback) {
    var query = models.User.findOne({});
    query.or([{username: name},{email: name}]);
    query.run(function(err, user) {
      if(err) return callback(err);
      if(!user) {
        return callback(null, false, {message: 'Unkown user'});
      }
      bcrypt.compare(password, user.hash, function(err, res) {
        if(err) return callback(err);
        if(!res) {
          return callback(null, false, {message: 'Invalid password'});
        }
        return callback(null, user);
      });
      return null;
    });
  };

  /**
   * retrives user by id
   * @param {String} userId
   * @param {Function} callback
   */
  var findById = function(userId, callback) {
    models.User.findOne({_id: userId}, callback);
  };

  /**
   * retrives user by email
   * @param {String} email
   * @param {Function} callback
   */
  var findByEmail = function(email, callback) {
    models.User.findOne({email: email}, callback);
  };

  /**
   * updates a user
   * @param {String} userId
   * @param {Object} userChanges
   * @param {Functon} callback
   */
  var update = function(userId, userChanges, callback) {

    if(userChanges.old_password || userChanges.new_password || userChanges.verify_password) {
      if(userChanges.old_password && userChanges.new_password && userChanges.verify_password && userChanges.new_password == userChanges.verify_password) {
        _password(userChanges.old_password,function(hash){
          if(user.hash == hash) {
            _password(userChanges.new_password,function(hash){
              models.User.update({_id: userId}, {hash: hash}, callback);
            });
          }
        });
      }
    } else {
      models.User.update({_id: userId}, userChanges, callback);
    }
  };

  /**
   * find or creates a new user from facebook
   */
  var findOrCreate = function(profile, callback) {
    models.User.findOne({facebookId: profile.id}, function(err, user) {
      if(err) callback(err);

      if(user) {
        callback(err, user);
      } else {

        // create username
        if(profile.username === 'undefined') {
          profile.username = displayName.replace(/\s/g, ".").displayName.toLowerCase();
        }

        var user = new models.User();
        user.facebookId = profile.id;
        user.username = profile.username;
        user.email = profile.emails[0].value;
        user.role = 'user';
        user.lang = i18n.lng();

        user.save(function(err, user) {
          callback(err, user);
        });

      }
    });
  };

  /**
   * return Object which is accessable to the outside
   */
  return {
    register: register,
    authenticate: authenticate,
    findById: findById,
    findByEmail: findByEmail,
    update: update,
    findOrCreate: findOrCreate
  };

}();

module.exports = User;
