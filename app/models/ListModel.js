var models = require('./models'),
    bcrypt = require('bcrypt'),
    _ = require('underscore');

var List = function() {

  /**
   * find list by id
   * @param {String} id
   * @param {Function} callback
   */
  var findById = function(id, callback) {
    models.User.findOne({'lists._id': id}, function(err, user) {
      callback(err, user.lists.id(id));
    });
  };

  /**
   * returns all lists from specific user plus official from admins
   * @param {String} userId
   * @param {Function} callback
   */
  var getAll = function(userId, callback) {
    models.User.find({$or: [{_id: userId}, {role: 'admin'}]}, function(err, users) {
      var lists = [];
      for(var i=0, len=users.length; i<len;i++) {
        for(var j=0, lenj=users[i].lists.length; j<lenj;j++) {
          var list = users[i].lists[j];
          lists.push(list);
        }
      }
      callback(err, lists);
    });
  };

  /**
   * returns all lists from specific user
   * @param {String} userId
   * @param {Function} callback
   */
  var getAllFromUser = function(userId, callback) {
    models.User.findOne({'_id': userId}, function(err, user) {
      callback(err, user.lists);
    });
  };

  /**
   * returns all lists from specific admin accounts
   * @param {Function} callback
   */
  var getAllFromAdmins = function(callback) {
    models.User.find({'role': 'admin'}, function(err, users) {
      var lists = [];
      for(var i=0, len=users.length; i<len;i++) {
        for(var j=0, lenj=users[i].lists.length; j<lenj;j++) {
          lists.push(users[i].lists[j]);
        }
      }
      callback(err, lists);
    });
  };

  /**
   * adds new list for specific user
   * @param {String} userId
   * @param {Object} list
   * @param {Object} nize
   * @param {Function} callback
   */
  var add = function(userId, list, callback) {

    // set defaults
    list || (list = {});
    list.name || (list.name = 'others');

    models.User.findOne({_id: userId}, function(err, user) {
      if(err) throw err;
      for(var i=0, len=user.lists.length; i<len; i++) {
        if(user.lists[i].name === list.name) {
          callback(err, user.lists[i]);
          return;
        }
      }
      var newList = new models.List(list);
      if(user.role === 'admin') {
        newList.open = true;
      }
      user.lists.push(newList);
      user.save(function(err) {
        callback(err, newList);
      });
    });
  };

  /**
   * adds a nize to a list
   * @param {String} listId
   * @param {Object} nize
   * @param {Function} callback
   */
  /*
  var addNizeToList = function(listId, nize, callback) {
    // check if nize has _id property !
    models.User.findOne({'lists._id': listId}, function(err, user) {
      var list = user.lists.id(list._id);
      list.push(nizeId);
      user.save(function(err) {
        callback(err, list);
      });
    });
  };
  */

  /**
   * renames a list
   * @param {String} id
   * @param {String} name
   * @param {Function} callback
   */
  var rename = function(id, name, callback) {
    // using UserModel because lists are embedded
    models.User.findOne({'lists._id': id}, function(err, user) {
      var list = user.lists.id(id);
      list.name = name;
      user.save(function(err, user) {
        callback(err, list);
      });
    });
  };

  /**
   * return Object which is accessable to the outside
   */
  return {
    add: add,
    rename: rename,
    findById: findById,
    getAllFromUser: getAllFromUser,
    getAll: getAll,
    getAllFromAdmins: getAllFromAdmins,
  };

}();

module.exports = List;
