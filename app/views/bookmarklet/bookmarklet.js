;(function (e, a, g, h, f, c, b, d) {
  if (!(f = e.jQuery) || g > f.fn.jquery || h(f)) {
    c = a.createElement("script");
    c.type = "text/javascript";
    c.src = "http://ajax.googleapis.com/ajax/libs/jquery/" + g + "/jquery.min.js";
    c.onload = c.onreadystatechange = function () {
      if (!b && (!(d = this.readyState) || d == "loaded" || d == "complete")) {
        h((f = e.jQuery).noConflict(1), b = 1);
        f(c).remove()
      }
    };
    i = 0;
    len = a.documentElement.childNodes.length;
    while (len--) {
      el = a.documentElement.childNodes[i];
      if (typeof el.tagName !== 'undefined' && el.tagName.toLowerCase() === 'head') {
        el.appendChild(c);
        break;
      }
      if (typeof el.tagName !== 'undefined' && el.tagName.toLowerCase() === 'body') {
        el.appendChild(c);
      }
      i++;
    }
  }
})(window, document, "1.7.2", function ($, L) {

  console.log('Bookmarklet for ' + window.weddynize.userId + ' loaded: jquery version: ', $.fn.jquery);

  /**
   * Underscore Binding function
   */
  var bind = function(func, obj) {
    var args = Array.prototype.slice.call(arguments, 2);
    return function() {
      var args2 = Array.prototype.slice.call(arguments);
      return func.apply(obj, args.concat(args2));
    };
  };

  /**
   * Mediator
   */
  var mediator = {

    channels: {},

    subscribe: function (channel, subscription) {
      if (!this.channels[channel]) this.channels[channel] = [];
      this.channels[channel].push(subscription);
    },

    publish: function (channel) {
      if (!this.channels[channel]) return;
      var args = [].slice.call(arguments, 1);
      for (var i = 0, l = this.channels[channel].length; i < l; i++) {
        this.channels[channel][i].apply(this, args);
      }
    }
  };


  /**
   * Bookmarklet Object
   */
  var wn_bookmarklet = {

    /**
     * Array of all scanned Nizes.
     */
    nizes: [],

    /**
     * Image you selected
     */
    image: false,

    /**
     * Prefix for css classes
     */
    prefix: "NIZE-" + (new Date).getTime() + '-',

    /**
     * Grid sizes
     */
    grid: {
      itemWidth: 230,
      gutter: 24,
      padding: 15,
      openNizeHeight: 484
    },

    /**
     * Config Object
     */
    config: null,

    /**
     * Bootstraps bookmarklet
     */
    init: function(config) {
      var css =  '';

      // save config file
      this.config = config;

      // load css for bookmarklet
      //href: window.weddynize.siteUrl + '/stylesheets/bookmarklet.css?x='+(Math.random()),
      css = this.config.cssRules.replace(/#-/g, this.prefix);
      css = css.replace(/#_cdn/g, this.config.siteUrl);
      $('<style>', {
        type: 'text/css'
      }).html(css).appendTo('body');

      // start scanning images
      this.scan();
    },

    /**
     * Scans page for images
     */
    scan: function() {
      var $images = $('img'),
          imageCount = $images.length,
          firstImage = false,
          self = this;

      if(!imageCount) {
        alert('No images');
      } else {

        // start building bookmarklet interface
        this.buildUI();

        var checkImage = function() {
          if(!--imageCount && !firstImage) {
            console.log('no images found');
            mediator.publish('noImages');
          }
        };

        // search for good images
        $images.each(function(k, v){
          var image = v,
              imageCheck = $('<img>');

          if(v.offsetHeight > 100 || v.offsetWidth > 100){
            // check if image is big enought
            (function(image){
              imageCheck.attr('src', image.src).load(function(){
                if(this.width >= 100 && this.height >= 100){

                  var nize = new self.Nize(this, self, k-1);

                  //self.images.push(this);
                  self.nizes.push(nize);

                  //self.addImage(k-1, this, nize);
                  self.addNize(k-1, nize, true);

                  firstImage = true;
                } else {
                  checkImage();
                }
              });
            })(image);
          } else {
            checkImage();
          }
        });
      }
    },

    /**
     * This Function creates the box-container
     */
    buildUI: function() {
      var scrollTop = $(document.body).scrollTop(),
          self = this;

      // save and set scrollPosition
      this.topHeight = $(window).scrollTop();
      $(window).scrollTop(0);

      // add a container and background for bookmarklet
      this.background = $('<div>', {'class': this.prefix+'background'}).appendTo(document.body);
      this.container = $('<div>', {'class': this.prefix+'bookmarklet'}).appendTo(document.body);
      this.body = $('<div>', {'class': this.prefix+'body'});
      this.body.click(function() {
        self.dispose();
      });

      /* build header */
      var $headerWrapper = $('<div>', {'class': this.prefix+'header'});
      // add content to header
      var $logo = $('<h1>', {'class': this.prefix+'logo'});
      var $logoLink = $('<a>', {'text': 'Weddynize'});
      $logo.append($logoLink);
      $headerWrapper.append($logo);
      var $title = $('<p>', {'class': this.prefix+'title', 'text': 'Add images to your collection'});
      $headerWrapper.append($title);
      var $cancelButton = $('<a>', {'class': this.prefix+'cancel', 'text': 'cancel'});
      $cancelButton.click(function() {
        self.dispose();
      });
      $headerWrapper.append($cancelButton);

      var $imageBox = $('<div>', {'class': this.prefix+'images-box'});
      $imageBox.appendTo(this.body);

      // add to site
      this.container.append($headerWrapper);
      this.container.append(this.body);

      // show no image found message
      mediator.subscribe('noImages', function() {
        self.body.append($('<p>', {text: 'no image found', 'class': self.prefix+'no-images-found'}));
      });

      // a nize is clicked to be opened
      mediator.subscribe('nize:clicked', function(nize) {
        //console.log(self.finishedCount, self.nizes.length);
        if(self.nizes.length == self.finishedCount) {
          self.calculateColumns(nize.column);
          self.foundCol = false;
          self.openNize = nize;
          self.nizeState = 'open';
          self.setGrid(true);
        }
      });

      // when a nize was added it gets removed
      mediator.subscribe('nize:remove', function(nize) {
        var idx = self.nizes.indexOf(nize);
        if(idx != -1) self.nizes.splice(idx, 1);
        console.log(self.nizes);
        nize.dispose();
        nize = null;
        self.openNize = null;
        self.setGrid(true);
      });

      // set grid
      this.setGrid();

      //window resize event
      var resizeTimer = null;
      $(window).resize(bind(function(event) {
        if(resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(bind(function() {
          if(this.nizeState !== 'open') {
            this.setGrid(true);
          }
        }, this), 100);
      }, this));
    },

    /**
     * creates parameters for grid
     */
    setGrid: function(relayout) {

      var viewPortWidth = $(window).outerWidth();
      var columnWidth = this.grid.itemWidth + this.grid.gutter;
      var columns = Math.floor((viewPortWidth - (this.grid.gutter*2))/columnWidth);
      // min 3 columns
      columns = columns < 3 ? 3 : columns;
      var containerWidth = Math.round(columns*columnWidth-this.grid.gutter);

      this.body.css('height', $('body').height());
      this.body.find('.' + this.prefix + 'images-box').css('width', containerWidth);

      // Prepare Array to store height of columns.
      this.heights = [];
      while(this.heights.length < columns) {
        this.heights.push(0);
      }

      this.columnWidth = columnWidth;
      this.columns = columns;

      // if position top is between grid values -> reposition nize
      if(this.openNize) {
        var rest = this.openNize.cssProp.top%this.columnWidth;
        this.openNize.cssProp.top = this.openNize.cssProp.top - rest;
      }

      this.finishedCount = this.nizes.length;

      if(relayout) {
        var self = this;
        this.finishedCount = 0;
        $.each(this.nizes, function(index, nize) {
          setTimeout(function() {
            self.addNize(index, nize);
          }, 0);
        });
      }
    },

    /**
     * Adds a image to the grid
     */
    addNize: function(index, nize, append) {

      // Find the shortest column.
      var shortest = null;
      var shortestIndex = 0;
      var self = this;
      var getColParameters = function() {
        shortest = null;
        shortestIndex = 0;
        for(k=0; k<self.columns; k++) {
          if(shortest === null || self.heights[k] < shortest) {
            shortest = self.heights[k];
            shortestIndex = k;
          }
        }
      };
      getColParameters();

      // if nize is open
      if(this.openNize && nize.index === this.openNize.index) {

        nize.css({
          position: 'absolute',
          top: this.openNize.cssProp.top,
          left: this.openColumns[0] * this.columnWidth
        });
        nize.bigSize();

      } else {

        // set height in right columns for open nize
        if(this.openNize && this.openNize.cssProp.top == shortest && !this.foundCol) {
          this.foundCol = true;
          var openColHeight = shortest + (this.openNize.outerHeight(true)) + this.grid.gutter;
          for(var i=0, len=this.openColumns.length; i<len; i++) {
            this.heights[this.openColumns[i]] = openColHeight;
          }
          getColParameters();
        }

        nize.css({
          position: 'absolute',
          top: shortest,
          left: shortestIndex * this.columnWidth
        });
        nize.smallSize(shortestIndex, index);

        // outerHeight doesnt always return right value,
        // so instead the fix value of itemWidth, height should also not be dynamic anyway
        var colHeight = shortest + this.grid.itemWidth + this.grid.gutter;
        this.heights[shortestIndex] = colHeight;

      }

      if(append) {
        nize.$el.appendTo(this.body.find('.' + this.prefix + 'images-box'));
      }


      // add nize to the pool
      this.finishedCount += 1;

      // position the last nize new of top is too much
      if(!append && index == this.nizes.length-1 && this.openNize) {
        var top = this.openColumns[0];
        for(i=0, len=this.openColumns.length; i<len; i++) {
          colHeight = this.heights[this.openColumns[i]];
          if(colHeight < this.openNize.cssProp.top) {
            top = (colHeight > top) ? colHeight : top;
          } else {
            return;
          }
        }
        this.openNize.css({
          top: top
        });
      }

    },

    /**
     * calculates columns for open nize
     */
    calculateColumns: function(column) {

      /* find columns */
      openColumns = [];

      if(column === 0) {
        openColumns[0] = column;
        openColumns[1] = column+1;
        openColumns[2] = column+2;
      } else if (column == this.columns-1) {
        openColumns[0] = column-2;
        openColumns[1] = column-1;
        openColumns[2] = column;
      } else {
        openColumns[0] = column-1;
        openColumns[1] = column;
        openColumns[2] = column+1;
      }

      this.openColumns = openColumns;

      return openColumns;

    },

    /**
     * Send the Nize to the server
     */
    submit: function(nize) {

      var self = this;

      Nize = nize.attributes();

      // sets nize infos
      var nize_info = {
        // lists
        lists: Nize.lists,
        // ratings
        rating: Nize.rating,
        // title
        title: Nize.title,
        // description
        description: Nize.description,
        // image
        image: nize.image.src,
        // from
        link: location.href,
        // referer
        referrer: document.referrer
      };

      // calls server
      $.ajax({
        url: this.config.siteUrl + '/bookmarklet/'+ this.config.userId,
        dataType: 'jsonp',
        data: nize_info,
        success: function(d){
          self.flash("success", "The image was saved to weddynize.com!");
          mediator.publish('nize:remove', nize);
        },
        error: function(d) {
        },
      });
    },

    flash: function(level, msg){

      var self = this;

      flash =   $('<div>', {'class': this.prefix + 'flash'});
      element = $('<div>', {'class': this.prefix + level});
      message = $('<p>');
      icon =    $('<span>', {'class': this.prefix + "icon"});
      text =    $('<span>', {'class': this.prefix + "text"});

      icon.text(level);
      text.text(msg);

      message.append(icon);
      message.append(text);
      element.append(message);
      flash.append(element);
      this.container.append(flash);

      this.container.animate({
        top: 42,
      }, 300);
      this.container.find("." + this.prefix + "header").animate({
        top: 42,
      }, 300);

      setTimeout(function(){
        self.unflash();
      }, 5000);

    },

    unflash: function(){
      this.container.animate({
        top: 0,
      }, 100);
      this.container.find("." + this.prefix + "header").animate({
        top: 0,
      }, 100);
      this.container.find("." + this.prefix + "flash").animate({
        top: 0,
      }, 100, function(){
        this.remove();
      });
    },

    /**
     * Nize contructor
     */
    Nize: function() {
      function Nize(image, obj, index) {

        this.minHeight = 511;
        this.imageMinWidth = 320;
        this.extraHeight = 60 + obj.grid.padding;

        this.obj = obj;
        this.image = image;
        this.originalImage = $.clone(image);
        this.index = index;


        // nize contents
        this.$el = $('<div>', {'class': obj.prefix+'nize-wrap'}).css('visibility', 'hidden');
        this.$el.click(function(evt) {
          evt.stopPropagation();
        });

        // add image
        this.$imageWrapper = $('<a>', {'class': this.obj.prefix+'nize-image-wrap'});
        this.$imageWrapper.append(image);
        this.$imageWrapper.appendTo(this.$el);

      }

      Nize.prototype.css = function(properties) {
        this.cssProp = properties;
        this.$el.css(properties);
      };

      Nize.prototype.clean = function() {
        this.$el.find('.' + this.obj.prefix+'nize-add-button').remove();
        this.$el.find('.' + this.obj.prefix + 'nize-form').remove();
        this.$el.find('.' + this.obj.prefix + 'nize-form-submit').remove();
        this.$imageWrapper.removeAttr('style');
      };

      Nize.prototype.loading = function(show) {
        this.$el.find('.' + this.obj.prefix+'nize-add-button').addClass('loading');
      };

      Nize.prototype.outerHeight = function(outer) {
        var imageHeight = Math.round(this.originalImage.height/this.originalImage.width * this.imageMinWidth) + this.extraHeight;
        var outerHeight = (imageHeight > this.minHeight) ? imageHeight : this.minHeight;
        if(outer) {
          return outerHeight + this.obj.grid.padding*2;
        }
        return outerHeight;
      };

      /**
       * shows nize in small
       */
      Nize.prototype.smallSize = function(column, index) {

        this.column = column;

        // clear html
        this.clean();

        // add properties
        this.$el.css({
          'visibility': 'visible',
          'width': 200,
          'height': 200,
          'padding': this.obj.grid.padding
        });

        // if horizontal format
        if(this.originalImage.height > this.originalImage.width) {

          // calculate image width with aspect ratio
          var imageHeight = Math.round(this.image.height/this.image.width * 200);

          $(this.$el.find('img')).css({
            'margin-top': -((imageHeight-200)/2),
            width: 200,
            height: imageHeight
          });

        // if vertical format
        } else {

          // calculate image width with aspect ratio
          var imageWidth = Math.round(this.image.width/this.image.height * 200);

          $(this.$el.find('img')).css({
            'margin-left': -((imageWidth-200)/2),
            height: 200,
            width: imageWidth
          });
        }

        // add nize button
        var $addButton = $('<button>', {'class': this.obj.prefix+'nize-add-button', 'text': 'Add', 'type': 'button'});
        var $spinner = $('<span>', {'class': this.obj.prefix+'nize-add-button-spinner'});
        $spinner.appendTo($addButton);
        this.$imageWrapper.append($addButton);

        // set click handler for a nize
        $addButton.click(bind(function() {
          this.loading(true);
          mediator.publish('nize:clicked', this);
        }, this));

      };

      /**
       * shows nize in an open state with formular
       */
      Nize.prototype.bigSize = function() {

        // clear html
        this.clean();

        this.$imageWrapper.css({
          width: this.imageMinWidth,
          height: 'auto'
        });

        // add properties
        this.$el.css({
          'width': (this.obj.grid.itemWidth * 3) + (this.obj.grid.gutter * 2) - (this.obj.grid.padding*2),
          'height': this.outerHeight(),
          'padding': this.obj.grid.padding
        });

        // calculate image width with aspect ratio
        $(this.$el.find('img')).css({
          width: this.imageMinWidth,
          margin: 'auto',
          height: Math.round(this.originalImage.height/this.originalImage.width * this.imageMinWidth)
        });

        // create form
        this.createForm();

      };

      Nize.prototype.attributes = function() {

        lists = $.map(this.$lists.find('input:checked'), function(v, k) {
          return $(v).val();
        });
        return {
          title: this.$title.val(),
          description: this.$description.val(),
          lists: lists,
          rating: this.$rating.data('rating')
        };
      };

      Nize.prototype.createForm = function() {

        var self = this;

        // add Formular
        var formWidth = (this.obj.grid.itemWidth*3) + this.obj.grid.gutter - this.imageMinWidth - (this.obj.grid.padding*2);
        var $form = $('<ul>', {'class': this.obj.prefix+'nize-form'}).css('width',formWidth);
        var appendToForm = function($el, klass) {
          var $li = $('<li>', {'class': self.obj.prefix+klass});
          $li.append($el);
          $form.append($li);
          return $li;
        };

        // add title input
        var $title = this.$title = $('<input>', {
          'class': this.obj.prefix+'nize-form-title',
          'type': 'text',
          'value': document.title,
          'placeholder': 'Name'
        });
        appendToForm($title);

        // add description input
        var $description = this.$description = $('<textarea>', { 'class': this.obj.prefix+'nize-form-description', 'placeholder': 'Description' });
        appendToForm($description);

        // add lists
        var $lists = this.$lists = $('<ul>', { 'class': this.obj.prefix+'nize-form-lists' });
        $newList = $('<input>', { 'class': this.obj.prefix+'nize-form-newlist', 'type': 'text', 'placeholder': 'Add new'});
        var $listsLi = appendToForm($lists);
        $newList.appendTo($listsLi);

        // handler for creating new lists
        $newList.keypress(function(evt) {
          var input = this;
          if(evt.which == 13) {
            // calls server
            $.ajax({
              url: self.obj.config.siteUrl + '/bookmarklet/'+ self.obj.config.userId + '/newlist',
              dataType: 'jsonp',
              data: {
                name: $(this).val()
              },
              success: function(list) {
                console.log(list);
                $(input).val('');
                addList(list, true);
              }
            });

          }
        });

        var addList = function(list, checked) {
          var posDouble = self.$el.find('#'+list._id);
          if(posDouble.length === 0) {
            var $list = $('<li>', { 'class': self.obj.prefix+'nize-form-list', 'data-id': list._id});
            var $listLabel = $('<label>', { 'for': list._id, text: list.name});
            var $listCheckbox = $('<input>', { 'id': list._id, 'value': list._id, 'type': 'checkbox'});
            if(checked) {
              $listCheckbox .attr('checked', true);
            }
            $listCheckbox.prependTo($listLabel);
            $listLabel.appendTo($list);
            $list.appendTo($lists);
          } else {
            posDouble.attr('checked', true);
          }
        };

        // Adding the lists
        $.each(this.obj.config.lists, function(id, list) {
          addList(list);
        });

        // add ratings
        var $rating = this.$rating = $('<ul>', { 'class': this.obj.prefix+'nize-form-rating'});
        var $rateCurrent = $('<li>', {'class': this.obj.prefix+'current'});
        $rateCurrent.appendTo($rating);
        $.each(this.obj.config.ratings, function(id,rating) {
          var $rateLi = $('<li>');
          var $rateA = $('<a>', { 'class': 'rate'+rating.value, 'title': rating.text, 'text': rating.value });
          $rateA.appendTo($rateLi);
          $rateLi.appendTo($rating);
          $rateA.click(function() {
            var rating = $(this).text();
            $rating.data('rating', rating);
            $rateCurrent.css('width', 100/4 * rating + '%');
          });
        });
        var $ratingLi = appendToForm($rating, 'rating');
        var $ratingText = $('<span>', {'text': 'Rating: '});
        $ratingText.prependTo($ratingLi);

        // add formular
        this.$el.append($form);

        // add Submit Button
        var $submitButton = $('<button>', {'class': this.obj.prefix+'nize-form-submit', 'text': 'Add to Weddynize'});
        this.$el.append($submitButton);
        $submitButton.click(function() {
          $(this).addClass(self.obj.prefix+'loading');
          self.obj.submit(self);
        });
      };

      Nize.prototype.dispose = function() {
        this.$el.remove();
      };

      return Nize;
    }(),

    /**
     * Removes the bookmarklet
     */
    dispose: function() {
      this.container.remove();
      this.background.remove();
      this.config.bookmarklet_running = false;
      // set scrollposition back
      $(window).scrollTop(this.topHeight);
    },

    /**
     * Helper Methods
     */
    $: function(selector) {
      return $('.' + this.prefix + selector);
    }

  };

  if(typeof window.weddynize.bookmarklet_running === 'undefined' || window.weddynize.bookmarklet_running === false){

    // bookmarklet is running
    window.weddynize.bookmarklet_running = true;

    // start bookmarklet
    wn_bookmarklet.init(window.weddynize);
  }

});
