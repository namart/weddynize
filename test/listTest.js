var mongoose = require("mongoose"),
    async = require('async'),
    List = require("../app/models/ListModel"),
    User = require("../app/models/UserModel"),
    Nize = require("../app/models/NizeModel"),
    models = require('../app/models/models');

mongoose.connect('mongodb://localhost/weddynize_test');

// CHANGE doc to better names

describe("Lists", function(){
  var currentUser = null;
  var currentUser2 = null;
  var currentList = null;
  var currentList2 = null;
  var currentNize = null;

  before(function(done){
    //delete all the user records
    models.User.remove({}, function() {
      done();
    });
  });

  beforeEach(function(done){

    async.series([
      function(callback){
        User.register("username", "test@test.com", "password", function(err, doc){
          currentUser = doc;
          callback(null, 'user1');
        });
      },
      function(callback){
        User.register("username2", "test2@test.com", "password", function(err, doc){
          currentUser2 = doc;
          callback(null, 'user2');
        });
      },
      function(callback) {
        List.add(currentUser._id, "list1", function(err, doc) {
          currentList = doc;
          callback(null, 'list1');
        });
      },
      function(callback) {
        List.add(currentUser2._id, "list2", function(err, doc) {
          currentList2 = doc;
          callback(null, 'list2');
        });
      },
      function(callback) {
        var nize = {
          title: 'nize title',
          description: 'nize description',
          lists: [currentList._id, currentList2._id]
        };
        Nize.add(currentUser._id, nize, function(err, doc) {
          currentNize = doc;
          callback(null, 'nize1');
        });
      },
      function(callback) {
        var nize = {
          title: 'nize title2',
          description: 'nize description',
          lists: [currentList._id]
        };
        Nize.add(currentUser2._id, nize, function(err, doc) {
          callback(null, 'nize2');
        });
      }
    ],
    function(err, results){
      done();
    });
  });

  afterEach(function(done){
    //delete all the user records
    models.User.remove({}, function() {
      done();
    });
  });

  it("adds a new list to the test@test.com user", function(done) {
    var list = {
      name: 'list2',
      open: false
    };
    List.add(currentUser._id, list, function(err, doc) {
      doc.name.should.equal('list2');
      done();
    });
  });

  it("should rename list1 to list3", function(done) {
    List.rename(currentList._id, "list3", function(err, doc) {
      doc.name.should.equal('list3');
      done();
    });
  });

  it("should add a new nize to the list1 with default list", function(done) {
    var nize = {
      title: 'nize title',
      description: 'nize description'
    };
    Nize.add(currentUser._id, nize, function(err, doc) {
      doc.title.should.equal('nize title');
      doc.description.should.equal('nize description');
      done();
    });
  });

  it("should return all nizes from user", function(done) {
    Nize.getAllFromUser(currentUser._id, function(err, doc) {
      for(var i=0; i<doc.length; i++) {
        doc[i]._creator.toString().should.equal(currentUser._id.toString());
      }
      done();
    });
  });

  it("should return specific nize by id", function(done) {
    Nize.getNizeById(currentNize._id, function(err, doc) {
      doc._id.toString().should.equal(currentNize._id.toString());
      done();
    });
  });

});
