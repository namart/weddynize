var mongoose = require("mongoose"),
    User = require("../app/models/UserModel"),
    models = require('../app/models/models');

mongoose.connect('mongodb://localhost/weddynize_test');

describe("Users", function(){
  var currentUser = null;

  before(function(done){
    //delete all the user records
    models.User.remove({}, function() {
      done();
    });
  });

  beforeEach(function(done){
    //add some test data    
    User.register("username", "test@test.com", "password", function(err, doc){
      currentUser = doc;
      done();
    });
  });

  afterEach(function(done){
    //delete all the user records
    models.User.remove({}, function() {
      done();
    });
  });

  it("registers a new user", function(done){
    User.register("username2", "test2@test.com", "password", function(err, doc){
      doc.email.should.equal("test2@test.com");
      doc.username.should.equal("username2");
      doc.hash.should.not.equal("password");
      done();
    });
  });

  it("authenticates with username and returns user with valid login", function(done) {
    User.authenticate("username", "password", function(err, user) {
      user.username.should.equal("username");
      done();
    });
  });

  it("authenticates with email and returns user with valid login", function(done) {
    User.authenticate("test@test.com", "password", function(err, user) {
      user.email.should.equal("test@test.com");
      done();
    });
  });

  it("find user by id", function(done) {
    User.findById(currentUser._id, function(err, doc) {
      doc.email.should.equal("test@test.com");
      done();
    });
  });

});
