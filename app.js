
/**
 * Module dependencies.
 */

var express = require('express'),
    nconf = require('nconf'),
    winston = require('winston'),
    mongoose = require('mongoose'),
    i18n = require('i18next'),
    mongoStore = require('connect-mongodb'),
    util = require('util'),
    fs = require('fs'),
    uploadDir = 'upload',
    tmpDir = 'tmp';


/**
 * Load Configs
 */
nconf.argv().env(); // First consider commandline arguments and environment variables, respectively.
nconf.file({ file: 'config/config.json' }); // Then load configuration from a designated file.

/**
 * Logging
 */
winston.add(winston.transports.File, {
  filename: 'logs/all-logs.log',
  handleExceptions: true
});

var logmessage = function(message) {
  message = '#' + (process.env.NODE_WORKER_ID ? process.env.NODE_WORKER_ID : 'M') + ': ' + message;
  if (winston) {
    winston.log('info', message);
  } else {
    console.log(message);
  }
};

/**
 * Start Server
 */
var app = module.exports = express.createServer();

/**
 * let's load app with more stuff
 */
app.config = nconf.get(process.env.NODE_ENV || 'development'); // Save config values in app.
app.settings = nconf.get('settings'); // Save settgins for the app.
app.logmessage = logmessage;

/**
 * connect to Mongodb when the app initializes
 */
mongoose.connect(app.config.db.url);

/**
 * Init Authentication strategies and aplieds passport to app variable
 */
require('./lib/authentication')(app);

/**
 * Configure i18n
 */
i18n.init({
  lng: 'en',
  fallbackLng: 'en',
});

/**
 * Configuration
 */
app.configure(function(){
  app.set('views', __dirname + '/app/views');
  app.set('view engine', 'jade');
  app.set('view options', { layout: 'layouts/default' });
  app.enable("jsonp callback");
  app.use(express.bodyParser({ uploadDir: tmpDir }));
  app.use(i18n.handle);
  app.use(express.methodOverride());

  app.use(express.cookieParser());
  app.use(express.session({
    secret: 'thisisweddynizesecret',
    store: new mongoStore({
      url: app.config.db.url,
      collection : 'sessions'
    })
  }));

  // Initialize passport
  app.use(app.passport.initialize());
  app.use(app.passport.session());

  app.use(app.router);
  app.use(express.static(__dirname + '/client/public', {maxAge: 86400000}));

  // static temporary file folder fo image uploads
  app.use('/tmp', express.static(__dirname + '/' + tmpDir));

});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

/**
 * check if tmp folder exists
 */
try {
  fs.lstatSync(tmpDir);
}
catch(e) {
  fs.mkdirSync(tmpDir, 0755);
}
app.set('tmpDir', tmpDir)


/**
 * Add Helper
 */
require('./app/helpers')(app);

i18n.registerAppHelper(app).serveClientScript(app).serveDynamicResources(app);

/**
 * Load app routes and up MVC Structure
 */
require('./app/')(app);

/**
 * Bootstrap custom error handler
 */
require('./error-handler').boot(app)

/**
 * Start Server
 */
var port = process.env.PORT || app.config.http.port;
app.listen(port, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, process.env.NODE_ENV || 'development');
});

