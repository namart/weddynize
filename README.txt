Installation guide
==================

Clone Repo & init
-----------------

1. npm install
2. cd client -> npm install
3. npm install brunch -g
4. brew install imagemagick
5. bew install mongodb
6. sudo mkdir -p /data/db
7. sudo chown `id -u` /data/db
8. sudo vim /ect/hosts -> testing.liechtenecker.at localhost

Start app
---------

1. mongod
2. ./bin/devclient
3. ./bin/devserver





Style guide
===========

HTML-Strukture
--------------

- header#header
  - div.inner.container_12
- section#content
  - div.inner.container_12
- footer#footer
  - div.inner.container_12

Grid-System
-----------

- .grid_1-12
- .container_12
- .push_1-12
- .pull_1-12
- .clear


CSS-Strukture
-------------

- mainelements
  - application (main element styles e.g. logo)
  - base        (base element styles e.g. hr)
  - grid        (grid system)
  - layout      (main layout e.g. header, content, footer, ...)
  - states      (transitions of elements. e.g. form-errors)

- modules (seperat objects of the website)

- shared (used by other stylesheets)
  - _colors (all color definitions)
  - _grid (nize-grid)
  
  