var mongoose = require("mongoose"),
    List = require("./app/models/ListModel"),
    User = require("./app/models/UserModel"),
    Nize = require("./app/models/NizeModel"),
    models = require('./app/models/models'),
    util = require('util'),
    fs = require('fs'),
    easyimg = require('easyimage'),
    async = require('async');

mongoose.connect('mongodb://localhost/weddynize_development');

async.series([
  function(callback){
    models.User.remove({}, function() {
      callback(null, 'one');
    });
  },
  function(callback) {
    models.Nize.remove({}, function() {
      callback(null, 'two');
    });
  }
],
function(err, results){

  /**
   * Adding User with LIsts and Nizes
   */
  User.register('admin', 'admin@weddynize.com', 'password', 'admin', function(err, user) {

    var lists = [];
    var nizes = [];

    async.series([
      /**
       * Adding Lists
       * */
      function(callback) {
        List.add(user._id, {
          name: 'liste1'
        }, function(err, list) {
          lists.push(list);
          callback();
        });
      },
      function(callback) {
        List.add(user._id, {
          name: 'liste2'
        }, function(err, list) {
          lists.push(list);
          callback();
        });
      },
      function(callback) {
        List.add(user._id, {
          name: 'liste3'
        }, function(err, list) {
          lists.push(list);
          callback();
        });
      },

      /**
       * Adding Nizes
       */
      function(callback) {
        var sum = 21;
        var counter = sum;
        for(var i=0, len=sum; i<len; i++) {

          Nize.add(user._id, {
            title: 'Nize'+i,
            description: 'Nize'+i+' Description',
            open: true,
            rating: randomXToY(1,4),
            lists: [lists[randomXToY(0,2)]._id, lists[randomXToY(0,2)]._id]
          }, function(err, nize) {

            // creates dynamically file location
            var imageUrl = function(type) {
              return 'tmp' + '/' + nize._id + '_' + type + '.png';
            };

            // Create Image
            var inp = fs.createReadStream('./image'+randomXToY(1,3)+'.png');
            var out = fs.createWriteStream(imageUrl('o'));
            util.pump(inp, out, function(){

              async.series([
                // create thumb
                function(callback) {
                  easyimg.thumbnail({
                      src: imageUrl('o'),
                      dst: imageUrl('s'),
                      width: '200',
                      gravity:  'North'
                    }, function(err, image) {
                      if (err) throw err;

                      nizeAdds = {
                        thumb: '/' + imageUrl('s'),
                        format: 'images/png'
                      };

                      Nize.updateNize(nize._id, nizeAdds, function(err) {
                        callback();
                      });
                  });
                },

                // create image
                function(callback) {
                  easyimg.info(imageUrl('o'), function(err, img) {

                    // resize to fit
                    if(img.width > 600) {
                      easyimg.resize({
                        src: imageUrl('o'),
                        dst: imageUrl('b'),
                        width: 600
                      }, function(err, image) {
                        if (err) throw err;

                        nizeAdds = {
                          original: '/' + imageUrl('b'),
                          size: '000'
                        };

                        Nize.updateNize(nize._id, nizeAdds, function(err) {
                          callback(err);
                        });
                      });
                    // take original
                    } else {
                      nizeAdds = {
                        original: '/' + imageUrl('o'),
                        size: '000'
                      };

                      Nize.updateNize(nize._id, nizeAdds, function(err) {
                        callback(err, true);
                      });
                    }
                  });
                }
              ], function(err, results) {
                //delete original
                if(!results[1]) {
                  fs.unlink(imageUrl('o'), function () {
                    if(--counter === 0) {
                      callback();
                    }
                  });
                } else {
                  if(--counter === 0) {
                    callback();
                  }
                }
              });
            });
          });
        }
      }
    ], function(err, results) { console.log('seeds addes for admin') });
  });



  /*
  User.register('test', 'test@test.at', 'test', function(err, user) {

    var lists = [];
    var nizes = [];

    async.series([

      function(callback) {
        List.add(user._id, {
          name: 'Meine erste Liste'
        }, function(err, list) {
          lists.push(list);
          callback();
        });
      },
      function(callback) {
        List.add(user._id, {
          name: 'Alles über Schuhe'
        }, function(err, list) {
          lists.push(list);
          callback();
        });
      },
      function(callback) {
        List.add(user._id, {
          name: 'Kleider'
        }, function(err, list) {
          lists.push(list);
          callback();
        });
      },


      function(callback) {
        var sum = 10;
        var counter = sum;
        for(var i=0, len=sum; i<len; i++) {

          Nize.add(user._id, {
            title: 'Mein NIze'+i,
            description: 'Meine Nize'+i+' Description',
            open: false,
            rating: randomXToY(1,4),
            lists: [lists[randomXToY(0,2)]._id, lists[randomXToY(0,2)]._id]
          }, function(err, nize) {
            if(--counter === 0) {
              callback();
            }
          });

        }

      }
    ], function(err, results) { console.log('seeds addes') });
  });
  */

});


/**
 * Helpers
 */


//function to get random number upto m
function randomXToY(minVal,maxVal,floatVal) {
  var randVal = minVal+(Math.random()*(maxVal-minVal));
  return typeof floatVal=='undefined'?Math.round(randVal):randVal.toFixed(floatVal);
}
