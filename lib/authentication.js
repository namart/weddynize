var User = require('../app/models/UserModel'),
   passport = require('passport'),
   FacebookStrategy = require('passport-facebook').Strategy,
   LocalStrategy = require('passport-local').Strategy;

var auth = function(app) {
  app.passport = passport;


  /**
   * Local Username/Password usage
   */
  passport.use(new LocalStrategy(function(username, password, done) {
    //callback matches with done's required params
    User.authenticate(username, password, done);
  }));

  passport.serializeUser(function(user, done) {
    done(null, user._id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });


  /**
   * facebook usage
   */
  passport.use(new FacebookStrategy({
    clientID: app.config.fb.appId,
    clientSecret: app.config.fb.appSecret,
    callbackURL: app.config.http.siteUrl + "/auth/facebook/callback"
  },function(accessToken, refreshToken, profile, done) {
    User.findOrCreate(profile, function (err, user) {
      return done(err, user);
    });
  }));

  /**
   * Simple route middleware to ensure user is authenticated.
   */
  app.ensureAuthenticated = function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/login');
  };

};

module.exports = auth;
