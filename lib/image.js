var fs = require('fs');
var util = require('util');
var easyimg = require('easyimage');
var knox = require('knox');

var client = knox.createClient({
  key:  process.env.AWS_ACCESS_KEY_ID,
  secret:  process.env.AWS_SECRET_ACCESS_KEY,
  bucket:  process.env.S3_BUCKET_NAME,
});

var image = function(app) {

  /*
   * creates thumbnails
   *
   * Creates a original and a thumbnail file for a given file.
   * Both are located in the tmp-folder. For long-time-storage the files
   * must be transported to the asset-server.
   *
   * @param file, the tmp-file to be saved
   * @param callback, callback function to be caled after completion
   *
   * @return hashmap with file-id and format
   */
  var create = function(file, tmp, width, callback) {

    /*
     * file infomation
     */
    var id = new Date().getTime();            //id, current timestamp
    var format = file.name.split('.').pop();  //format of the file

    /*
     * dynamic imageUrl-string dep. on type.
     */
    var imageUrl = function(type) {
      return tmp + '/' + id + '_' + type + '.' + format;
    };

    /*
     * move the original file to the tmp location.
     */
    var inp = fs.createReadStream(file.path);       //read stream
    var out = fs.createWriteStream(imageUrl('o'));  //write stream

    util.pump(inp, out, function(){

      /*
       * delete the original file, after copying it to tmp location.
       */
      fs.unlink(file.path, function () {

        /*
         * create thumbnail form original.
         */
        easyimg.thumbnail({
          src: imageUrl('o'),
          dst: imageUrl('s'),
          width: width
        }, function(err, image) {

          /*
           * after all we can return file infos now.
           */
          callback(err, {
            original: '/' + imageUrl('o'),
            thumb: '/' + imageUrl('s'),
            format: format,
            type: file.type,
            size: file.size,
          });

          /*
           * GC should delete the file
           */
          file = null;
      });
     });
    });
  };

  /*
   * transport file
   *
   * Transports a file to the asset-server and delete the source file
   *
   * @param location, the source-location of the file to transport
   * @param name, the destination name of the asset
   * @param callback, callback function to be caled after completion
   *
   * @return hashmap with file-id and format
   */
  var transport = function(location, name, callback) {

    if(!location || !name) return false;

    /*
     * remove trailing slash
     */
    if(location.substr(0, 1) == "/") {
      location = location.substr(1);
    }

    /*
     * upload the file to the assetserver
     */
    client.putFile(location, name, function(err, res){
      if(err) throw err;

      /*
       * after transportation we can delete the source file.
       */
      fs.unlink(location, function (err) {
        callback(err, {name: name});
      });
    });
  };



  /*
   * return Object which is accessable to the outside
   */
  return {
    create: create,
    transport: transport,
  };
}();

module.exports = image;
