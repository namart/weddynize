var application = require('application');

module.exports = Backbone.Router.extend({

  routes: {
    '' : 'home',
    'all': 'homeAll',
    'nize/add' : 'addNize',
    'nize/:id' : 'nize',
    '_=_' : 'facebookLoginBug'
  },

  home: function() {
    Backbone.Mediator.publish('nizeLayoutView:render');
  },

  homeAll: function() {
    Backbone.Mediator.publish('nizeLayoutView:render', true);
  },

  addNize: function() {
    Backbone.Mediator.publish('addNize:init');
  },

  nize: function(nizeId) {
    Backbone.Mediator.publish('openNize:init', nizeId);
  },

  facebookLoginBug: function() {
    this.navigate("/", {trigger: true, replace: true});
  }

});
