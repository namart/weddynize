var application = require('application');

$(function() {

  var options = {
    lng: application.lang,
    useLocalStorage: false,
    resGetPath: '/locales/resources.json?lng=__lng__&ns=__ns__',
    dynamicLoad: true,
    sendMissing: false
  };

  //First of all, we need to load locales, after that we can do all the rest
  $.i18n.init(options, function() {
    application.initialize();
    Backbone.history.start({pushState: true});
  });


  //This is a little form helper to remove form-labels when user clicks in input field
  $("input.error").focus(function(){
    $(this).parent().find("label.errormessage").fadeOut(200);
  });

});
