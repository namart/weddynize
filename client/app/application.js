// Application bootstrapper.
Application = {
  initialize: function() {

    // load in App data
    _.extend(this, App);

    var Router = require('lib/router');
    var ViewHelper = require('lib/view_helper');
    var HeaderView = require('views/headerView');
    var AddNizeView = require('views/nize/addNizeView');
    var NizeLayoutView = require('views/nizeLayoutView');

    this.headerView = new HeaderView();
    var nizeLayoutView = new NizeLayoutView();
    var addNizeView = new AddNizeView();

    this.router = new Router();

    if (typeof Object.freeze === 'function') Object.freeze(this);
  }
};

module.exports = Application;
