var Model = require('./model');

module.exports = Model.extend({

  idAttribute: '_id',

  urlRoot: '/list',

  defaults: function() {
    return {
      open:  false,
      checked : false
    };
  }

});
