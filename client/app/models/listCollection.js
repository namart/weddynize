var Collection = require('./collection');
var ListModel = require('./listModel');

module.exports = Collection.extend({

  url: '/list',

  model: ListModel,

  showByAttribute: function(attr) {
    var filteredLists = this.where(attr);
    this.trigger('filteredLists', filteredLists);
  }

});
