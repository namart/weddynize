var Collection = require('./collection');
var NizeModel = require('./nizeModel');

module.exports = Collection.extend({

  urlRoot: '/nize',

  model: NizeModel,

  initialize: function() {

    this.listFilter = '';
    this.listFilterName = '';
    this.ratingFilter = '';
    this.keyFilter = '';
    this.open = false;
    this.limit = 20;

    this.setSearchDefaults();
  },

  setSearchDefaults: function() {
    this.offset = 0;
    this.endReached = false;
  },

  setListFilter: function(listId, listName) {
    console.log(listName);
    this.setSearchDefaults();
    this.listFilter = listId;
    this.listFilterName = listName;
    this.search();
  },

  setRatingFilter: function(ratingValue) {
    this.setSearchDefaults();
    this.ratingFilter = ratingValue;
    this.search();
  },

  setKeyFilter: function(key) {
    this.setSearchDefaults();
    this.keyFilter = key;
    this.search();
  },

  setOpenFilter: function(state) {
    this.setSearchDefaults();
    this.open = state;
    this.search();
  },

  getNext: function() {
    this.offset += this.limit;
    this.search();
  },

  getFilters: function() {
    if(this.listFilter || this.ratingFilter || this.keyFilter) {
      return {
        list: this.listFilterName,
        rating: this.ratingFilter,
        key: this.keyFilter
      };
    } else {
      return null;
    }
  },

  search: function(data) {
    if(this.endReached) return;
    var url = this.urlRoot + '/search.json';
    var self = this;
    data = _.extend({
      key: this.keyFilter,
      list: this.listFilter,
      rating: this.ratingFilter,
      limit: this.limit,
      open: this.open,
      offset: this.offset
    },data);

    $.ajax({
      url:url,
      data: data,
      dataType:"json",
      success:function (data) {
        //console.log("search success: " + data.length, data);

        // append or reset
        if(self.offset === 0) {
          self.reset(data);
        } else {
          self.add(data);
        }

        // send event if all data already loaded
        if(data.length < self.limit) {
          self.endReached = true;
          self.trigger('finished');
        } else {
          self.trigger('newSearch');
        }
      }
    });
  }

});
