var View = require('../view');
var NizeUnitView = require('./nizeUnitView');
var noNizeFoundView = require('../search/noNizeFoundView');
var application = require('application');

module.exports = View.extend({

  gridOptions: {
    itemWidth: 232,
    gutter: 24
  },

  initialize: function() {
    //console.debug('init#nizeView', this.collection);

    // Layout variables.
    this.columns = null;
    this.columnWidth = null;
    this.heights = [];

    //More Nize index holder
    this.nizeIndex = 0;

    //Bindings
    this.bindTo(this.collection, 'reset', this.addAll);
    this.bindTo(this.collection, 'add', this.addOne);
    this.bindTo(this.options.parent, 'more', function(){ this.nizeIndex = 0;});
    this.bindTo(this.collection, 'destroy', function(){
      this.addAll;
      this.addAll(this.collection);
    });
    
    //we need to add this margin, for the scrolling effect
    $("#header").css({"margin-bottom": "55px"});
    
    //window resize event
    var resizeTimer = null;
    $(window).resize(_.bind(function(event) {
      if(resizeTimer) clearTimeout(resizeTimer);
      resizeTimer = setTimeout(
        _.bind(
          this.layout, 
          this, 
          this.$el.find('.nize').length < 7 ? 1020 : null
        ), 100
      );
    }, this));
  },

  layout: function(maxContainerWidth) {

    // Calculate basic layout parameters.
    var containerWidth = maxContainerWidth || $(window).outerWidth();
    this.columnWidth = this.gridOptions.itemWidth + this.gridOptions.gutter;
    
    var columns = Math.floor((containerWidth + this.gridOptions.gutter)/this.columnWidth);
    
    // minimal 4 columns
    if(columns < 4) columns = 4;
    
    var oldColumns = this.columns;
    this.columns = columns = columns%2 ? (columns-1<4 ? columns : columns-1) : columns;

    var newContainerWidth = Math.round(columns*this.columnWidth-this.gridOptions.gutter);
    this.$el.css('width', newContainerWidth);


    // Prepare Array to store height of columns.
    this.heights = [];
    while(this.heights.length < columns) {
      this.heights.push(0);
    }

    // resize items if exists
    if(this.$el.find('.nize').length && oldColumns !== columns) {
      this.disposeViews();
      this.collection.each(_.bind(this.addOne, this));
    }
  },

  addAll: function(nizes) {
    this.disposeViews();
    this.layout(nizes.length < 7 ? 1020 : null);
    if(nizes.length === 0) {
      this.renderView(this.$el, 'append', new noNizeFoundView({parent: this, filters: nizes.getFilters()}));
    } else {
      this.nizeIndex = 0;
      nizes.each(_.bind(function(nize, counter) {
        this.addOne(nize, nizes, {index: counter});
      }, this));
    }
  },

  addOne: function(nize, nizes, options) {

    var index = options.index;
    if(this.nizeIndex === 0) this.nizeIndex = index;
    options.index = index - this.nizeIndex;

    var self = this;

    // Find the shortest column.
    var shortest = null;
    var shortestIndex = 0;

    for(k=0; k<this.columns; k++) {
      if(shortest === null || this.heights[k] < shortest) {
        shortest = this.heights[k];
        shortestIndex = k;
      }
    }
    
    var cssChanges = {
      position: 'absolute',
      top: shortest,
      left: shortestIndex * self.columnWidth, 

    };

    var nizeUnitView = new NizeUnitView({
      model:nize,
      css: cssChanges,
      id: nize.get('_id'),
      index: options.index
    });
    this.renderView(this.$el, 'append', nizeUnitView);
      
    var colHeight = shortest + nizeUnitView.$el.outerHeight() + this.gridOptions.gutter;
    this.heights[shortestIndex] =  colHeight;
    this.$el.css('height', colHeight);
  }

});
