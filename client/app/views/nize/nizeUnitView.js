var View = require('../view');
var template = require('../templates/nize/nizeUnit');
var ratingTemplate = require('../templates/shared/rating');
var application = require('application');

module.exports = View.extend({

  className: 'nize',

  template: template,

  getRenderData: function() {
    return {
      nize: this.model.toJSON()
    };
  },

  events: {
    "click  a.nize-image": "openNize"
  },

  initialize: function() {    
    //console.log('order: ', this.options.order);
    var cssOptions = _.extend({opacity: 0}, this.options.css);
    this.$el.css(cssOptions);
    
    this.bindTo(this.model, 'change', this.updateView);

    // fade in depending on the order
    setTimeout(_.bind(function() {
      this.$el.css('opacity', 1);
    }, this), 50*this.options.index);
  },

  afterRender: function() {
    var perc = 100/4*this.model.get('rating');
    this.$el.append(ratingTemplate({
      ratings: application.ratings,
      rating: perc,
      current: this.model.get('rating'), 
      notActive: true, 
    }));
  },

  openNize: function(evt) {
    evt.preventDefault();
    application.router.navigate('nize/' + this.model.get('_id'), {trigger: true});
  }, 
  
  updateView: function(obj) {
    this.$el.find("*[role=title]").text(obj.get("title"));
    this.$el.find("*[role=description]").text(obj.get("description"));
    
    this.$el.find("ul.rating .current").text("" + obj.get("rating") + " out of 4");
    this.$el.find("ul.rating .current").width("" + obj.get("rating")*25 + "%");
  }, 

});
