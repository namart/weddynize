var View = require('../view');
var Nize = require('models/nizeModel');
var List = require('models/listModel');
var ListCollection = require('models/listCollection');
var template = require('../templates/nize/addNizeForm');
var ListUnitView = require('../list/listUnitView');
var application = require('application');

module.exports = View.extend({

  el: '.add-nize-right',

  template: template,

  initialize: function() {
    //console.debug('init#addNize', this.collection);

    _.bindAll(this, 'addOneList');

    this.c = document.createElement( 'canvas' );
    this.img = null;
    this.file = null;
    this.lists = null;

    // supported file types
    this.fileSupport = ['image/jpeg', 'image/png'];
  },

  events: {
    'change .fileselect' : 'fileSelectHandler',
    'dragover .filedrag' : 'fileDragHover',
    'dragleave .filedrag' : 'fileDragHover',
    'drop .filedrag' : 'fileSelectHandler',
    'keypress .nize-new-list' : 'newList',
    'click .nize-submit' : 'createNize'
  },

  subscriptions: {
    'addNize:init': 'init'
  },

  init: function() {

    // only if browser supports File API
    if (window.File && window.FileList && window.FileReader) {

      var xhr = new XMLHttpRequest();
      if(xhr.upload) {
        this.$el.find('.filedrag').css('display', 'block');
        this.$el.find('.submitbutton').css('display', 'none');
      }

    } else {
      this.undelegateEvents();
    }
  },

  fileDragHover: function(e) {
    e.stopPropagation();
    e.preventDefault();
    if(e.type == "dragover") {
      $(e.target).addClass("hover");
    } else {
      $(e.target).removeClass("hover");
    }
  },

  fileSelectHandler: function(e) {

    // cancel event and hover styling
    this.fileDragHover(e);

    // fetch FileList object
    var files = e.target.files || e.originalEvent.dataTransfer.files;

    var file = files[0];
    this.file = file;
    if ( file.type.indexOf( 'image' ) === 0 ) {
      this.showNizeForm();
    } else {
      alert('Only Images allowed');
    }

  },

  showNizeForm: function() {
    var url = window.URL || window.webkitURL,
        self = this,
        data = null,
        img = new Image(),
        reader = new FileReader(),
        objURL = url.createObjectURL || false;

    this.lists = new ListCollection(application.lists);
    this.bindTo(this.lists, 'add', this.addOneList);
    this.$el.find('.upload').css('display', 'none');
    this.$el.append(this.template( { lists: this.lists.toJSON() } ));

    // add lists
    _.each(this.lists.models, _.bind(function(list) {
      this.renderView(this.$el.find('.nize-lists'), 'append', new ListUnitView({ model: list }));
    }, this));

    // add rating
    _.each(application.ratings, _.bind(function(rating) {
      this.$el.find('.nize-rating select').append($('<option>', {value: rating.value, text: rating.text}))
    }, this));

    // show image
    if(objURL) {
      img.src = url.createObjectURL(this.file);
      this.$el.prepend(img);
    } else {
      reader.readAsDataURL( this.file );
      reader.onload = function ( ev ) {
        img.src = ev.target.result;
        img.onload = function() {
          self.$el.prepend(this);
        };
      };
    }

  },

  addOneList: function(list) {
    this.renderView(this.$el.find('.nize-lists'), 'append', new ListUnitView({ model: list }));
  },

  newList: function(evt) {
    if(evt.which == 13) {
      var listName = this.$el.find('.nize-new-list input').val();
      var self = this;
      var list = new List();
      list.save({ name: listName, checked: true }, {
        wait:true,
        success:function(model, response) {
          self.lists.add(model);
        },
        error: function(model, error) {
          console.log(model.toJSON());
          console.log('error.responseText');
        }
      });
    }
  },

  createNize: function() {
    var title = this.$el.find('.nize-title input').val();
    var rating = this.$el.find('.nize-rating select').val();
    var description = this.$el.find('.nize-description input').val();
    var self = this;
    var lists = $('.nize-lists input:checked');

    lists = $.map(lists, function(v, k) {
      return $(v).val();
    });

    var nizeObj = {
      title: title,
      description: description,
      lists: lists,
      rating: rating,
      link: ''
    };

    this.nize = new Nize();
    this.nize.save(nizeObj, {
      wait:true,
      success:function(model, response) {
        console.log('successfully added nize');
        self.uploadImage();
      },
      error: function(model, error) {
        console.log(model.toJSON());
        console.log('error.responseText');
      }
    });

  },

  uploadImage: function() {
    var xhr = new XMLHttpRequest();

    if (xhr.upload && this.fileSupport.indexOf(this.file.type) > -1 && this.file.size <= this.$el.find("#MAX_FILE_SIZE").val()) {

      // progress bar
      xhr.upload.addEventListener("progress", function(e) {
        var pc = parseInt(100 - (e.loaded / e.total * 100), 10);
        debug.log(pc + "%");
      }, false);

      // file received/failed
      xhr.onreadystatechange = function(e) {
        if (xhr.readyState == 4) {
          debug.log(xhr.status == 200 ? "success upload" : "upload failure");
        }
      };

      var formdata = new FormData();
      formdata.append(this.file.name, this.file);
      formdata.append('nizeId', this.nize.get('_id'));

      // start upload
      xhr.open("PUT", this.$el.find('.upload').get(0).action, true);
      xhr.setRequestHeader("X_FILENAME", this.file.name);
      xhr.send(formdata);

    }
  }

});
