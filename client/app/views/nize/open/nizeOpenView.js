var View = require('../../view');
var NizeOpenUnitView = require('./nizeOpenUnitView');
var application = require('application');

module.exports = View.extend({

  className: 'nize-open',

  events: {
    "click": "close"
  },

  initialize: function() {
    //console.debug('init#nizeOpen', this.collection);
    
    this.bindTo(this.collection, 'destroy', this.close);
    
    var $nizeElement = $('#'+this.options.nizeId);
    var startPosition = $nizeElement.offset();
    var self = this;

    // calculate top for scrolled states
    startPosition.top = startPosition.top - $(document).scrollTop();

    // Set page to the background
    this.scrollBarWidth = this.getScrollBarWidth();
    $('body').addClass('noscroll');
    $('body').css('margin-right', this.scrollBarWidth);
    $('.l-search-controls-inner').css('width', $('.l-search-controls-inner').width() + this.scrollBarWidth);

    // add open nize
    this.nizeOpenUnitView = new NizeOpenUnitView({css: startPosition, model: this.collection.get(this.options.nizeId)});
    this.bindTo(this.nizeOpenUnitView, 'imgLoaded', this.open);
    this.renderView(this.$el, 'append', this.nizeOpenUnitView);

    $('body').append(this.$el);

    //start loading state
    this.$el.addClass('loading');

    },

  open: function() {

    // start visible state
    this.$el.addClass('visible');

    // center nize
    var viewportWidth = $(document).outerWidth();
    this.nizeOpenUnitView.$el.css('left', viewportWidth/2 - 326 - (this.scrollBarWidth/2));

    // set css properties for nize opening animation
    this.nizeOpenUnitView.$el.find('img').css('margin-left', 0);

  },

  close: function(evt) {
    // stop loading nize when hin as target
    
    if(typeof(evt.preventDefault) == "function") {   
      evt.preventDefault();
    }

    // Bring Backgroun back to front
    $('body').removeClass('noscroll');
    $('body').css('margin-right', 0);
    $('.l-search-controls-inner').css('width', $('.l-search-controls-inner').width() - this.scrollBarWidth);

    application.router.navigate('/');
    this.dispose();
  },

  //Helper
  getScrollBarWidth: function() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild (inner);

    document.body.appendChild (outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild (outer);

    return (w1 - w2);
  }

});
