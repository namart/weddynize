var View = require('../../view');
var application = require('application');
var template = require('../../templates/nize/open/nize');
var ratingTemplate = require('../../templates/shared/rating');

module.exports = View.extend({

  className: 'nize-open-unit nize',

  template: template,

  getRenderData: function() {
    return {
      nize: this.model.toJSON()
    };
  },

  events: {
    "click .nize-field-edit"  : "inlineEditBegin",
    "blur .nize-field-edit"   : "inlineEditEnd",
    "click a.delete"          : "deleteNize",
    "click ul.rating"         : "changeRating",
    "click"                   : "preventClosing",
    //"keydown .nize-field-edit" : "preventMultiline",
  },

  initialize: function() {
    //console.debug('init#nizeOpenUnit', this.collection);
    this.$el.css(this.options.css);

    if(this.canEditNize()) {
      this.model.set("editable", true);
    }
  },

  afterRender: function() {

    // Load big picture
    var $image = this.$el.find('img');
    var img = new Image();
    var self = this;
    $(img).bind('load error', function(e){
      //now its loaded so show it
      $image.css('visibility', 'visible');
      // setting css properties for nice opening animation
      $image.css('margin-left',-($image.width()-200)/2);
      $image.css('height', img.height);
      //notify as ready
      self.trigger('imgLoaded');
      //wait for animation
      setTimeout(function() {
        self.loadContent();
      }, 300);
    });
    img.src = $image.attr('src');
  },

  loadContent: function() {

    this.$el.parent().removeClass('loading').addClass('loaded');

    // show rating
    var perc = 100/4*this.model.get('rating');
    this.$el.find('.nize-open-header').append(ratingTemplate({
      ratings: application.ratings,
      rating: perc,
      notActive: !this.canEditNize(),
    }));

    if(this.canEditNize) {
      this.$el.find('.nize-field-edit').addClass('show-edit');
      this.$el.find('.nize-field-edit').attr("contenteditable", true);

      setTimeout(_.bind(function() {
        this.$el.find('.nize-field-edit').removeClass('show-edit');
      }, this), 1000);
    }
  },

  canEditNize: function() {
    if(application.user == this.model.get("_creator")) {
      return true;
    }

    return false;
  },

  preventClosing: function(evt) {
    evt.stopPropagation();
  },

  inlineEditBegin: function(evt) {
    if(this.canEditNize()) {
      //evt.target.setAttribute("contenteditable", true);
    }
  },

  inlineEditEnd: function(evt) {
    if(this.canEditNize()) {

      // disable content editable
      //evt.target.setAttribute("contenteditable", false);

      this.highlight(evt.target);

      // get element role and change attribute if valid
      role = evt.target.getAttribute("role");
      if(this.model.has(role)) {
        this.model.save(role, evt.target.innerHTML, {error: this.errorOnSave});
      }
    }
  },

  changeRating: function(evt) {
    if(this.canEditNize()) {
      if(evt.target.hasAttribute("data-rating-value")) {
        this.model.save("rating", evt.target.getAttribute("data-rating-value"), {error: this.errorOnSave});
        this.$el.find("ul.rating").removeClass("no-active");
        this.$el.find("ul.rating .current").text("" + this.model.get("rating") + " out of 4");
        this.$el.find("ul.rating .current").width("" + this.model.get("rating")*25 + "%");
      }
    }
  },

  errorOnSave: function(model, response) {
    alert("ERROR WHILE SAVING!");
  },

  preventMultiline: function(evt) {
    if(evt.keyCode == 13) {
      evt.stopPropagation();
      return false;
    }
  },

  deleteNize: function(evt) {

    if (confirm('Are you sure you want to delete this nize?')) {
      this.model.destroy({wait: true});
    }
  },

  highlight: function(obj, error){
    $(obj).addClass("highlight");

    if(error) {
      $(obj).addClass("error");
    }

    setTimeout(function(){
      $(obj).removeClass("highlight");
      $(obj).removeClass("error");
    }, 200);
  }

});
