var View = require('../view');
var List = require('/models/listModel');
var ListUnitView = require('./listUnitView');
var template = require('../templates/search/dropdown');

module.exports = View.extend({

  className: 'dropdown',

  template: template,

  getRenderData: function() {
    return {
      title: "Category:",
      defaultValue: "All"
    };
  },

  events: {
    "click a.current": "toggleBox"
  },

  initialize: function() {
    _.bindAll(this, 'addOne', 'addAll', 'selected');
    this.bindTo(this.collection, 'filteredLists', this.addAll);
  },

  addAll: function(lists) {
    this.disposeViews();
    this.addOne(new List({_id: '0', name: 'all'}));
    _.each(lists, this.addOne);
  },

  addOne: function(list) {
    var listUnitView = new ListUnitView({ model: list });
    this.bindTo(listUnitView, 'selection', this.selected);
    this.renderView(this.$el.find('ul'), 'append', listUnitView);
  },

  toggleBox: function(evt) {
    var self = this;
    evt.stopPropagation();
    if(this.$el.hasClass('open')) {
      this.closeBox();
    } else {
      this.$el.addClass('open');
      $(document).bind('click', function(evt) {
        self.closeBox();
      });
    }
    return false;
  },

  closeBox: function() {
    this.$el.removeClass('open');
  },

  selected: function(list) {
    this.$el.find('.current').html(list.$el.html());
    this.trigger('selection', list.$el.data('list-id'), list.$el.text());
  }

});
