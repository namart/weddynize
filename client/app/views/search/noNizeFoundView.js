var View = require('../view');
var template = require('../templates/nize/noNize');

module.exports = View.extend({

  className: 'no-nize-found',

  template: template,

  getRenderData: function() {
    return {
      filters: this.options.filters
    };
  },

  afterRender: function() {
    this.options.parent.$el.css('height', 'auto');
  }

});
