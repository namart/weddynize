var View = require('../view');

module.exports = View.extend({

  tagName: 'li',

  className: 'text-type',

  events: {
    "click": "selected"
  },

  attributes: function() {
    return {
      'data-list-id': this.model.get('_id') ? this.model.get('_id') : 0
    }
  },

  render: function() {
    $(this.el).html(this.model.get('name'));
    this.afterRender();
    return this;
  },

  selected: function() {
    this.trigger('selection', this);
  }

});
