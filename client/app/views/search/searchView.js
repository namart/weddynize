var View = require('../view');
var template = require('../templates/search/searchForm');

module.exports = View.extend({

  className: 'search',

  template: template,

  events: {
    'keyup': 'newSearch'
  },

  initialize: function() {

    this.counter = 10;
    this.searched = false;

  },

  newSearch: function() {
    this.counter = 10;
    var self = this;
    var time = 30;
    var timerCallback = function() {
      self.counter--;
      if(self.counter === 0) {
        self.searched = false;
        self.trigger('newSearch', self.$el.find('input').val());
      } else {
        setTimeout(timerCallback, time);
      }
    };
    if(this.searched === false) {
      this.searched = true;
      setTimeout(timerCallback, time);
    }
  }

});
