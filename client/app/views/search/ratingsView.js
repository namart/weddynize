var View = require('../view');
var application = require('application');
var ratingTemplate = require('../templates/shared/rating');

module.exports = View.extend({

  tagName: 'div',

  className: 'ratingfilter',

  events: {
    "click": "removeRate",
    "click .rating": "selection"
  },

  initialize: function() {
    this.$el.addClass("bordered");
  },

  afterRender: function() {
    var description = $('<label>', {text:'Rating: '});
    this.$el.append(description);
    this.$el.append(ratingTemplate({ratings: application.ratings, rating: 0}));
  },

  removeRate: function() {
    this.setRate(0);
    this.trigger('selection', 0);
    return false;
  },

  selection: function(evt) {
    evt.stopPropagation();
    var rate = $(evt.target).text();
    this.setRate(rate);
    this.trigger('selection', rate);
    return false;
  },

  setRate: function(rate) {
    var perc = 100/4*rate;
    this.$el.find('.current').css('width', perc+'%');
  }

});
