var View = require('../view');
var template = require('../templates/list/listUnit');

module.exports = View.extend({

  template: template,

  getRenderData: function() {
    return {
      list: this.model.toJSON()
    };
  }

});
