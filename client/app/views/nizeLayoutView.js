var View = require('./view');
var application = require('../application');
var NizeCollection = require('../models/nizeCollection');
var ListCollection = require('../models/listCollection');
var NizesView = require('./nize/nizesView');
var NizeOpenView = require('./nize/open/nizeOpenView');
var searchControlsView = require('./searchControlsView');

module.exports = View.extend({

  el: '.nize-layout',

  events: {
    "click .load-more": "moreNizes"
  },

  subscriptions: {
    'nizeLayoutView:render': 'load',
    'openNize:init': 'openNize'
  },

  load: function(open) {

    // set open state depending if the user is logged in
    this.open = application.user ? false : true;

    // If url points to /all then show open nizes
    if(open) this.open = true;
    
    if (this.open) {
      this.manageMenu("/all");
    } else {
      this.manageMenu("/");
    }

    this.nizes || (this.nizes = new NizeCollection());
    this.lists || (this.lists = new ListCollection(application.lists));

    // bindings
    this.bindTo(this.nizes, 'finished', this.endNizes);
    this.bindTo(this.nizes, 'newSearch', this.newNizes);

    // sub views
    this.nizesView || (this.nizesView = new NizesView({ 
      el: this.$('.l-nizes'),
      collection: this.nizes,
      parent: this
    }));

    this.searchControlsView || (this.searchControlsView = new searchControlsView({ 
      el: $('.l-search-controls-inner'),
      listCollection: this.lists,
      nizeCollection: this.nizes
    }));

    // Set filter for the right nizes
    this.nizes.setOpenFilter(this.open);
    // start fetching lists
    this.lists.showByAttribute({open: this.open});

  },
  
  manageMenu: function(url) {
    
    //reset all menu items
    var items = $("nav.top-menu.filter ul li a");
    items.addClass("small");
    items.addClass("lightgrey");
    items.removeClass("blue");
    
    //mark current element
    var items = $('nav.top-menu.filter ul li a[href="' + url + '"]');
    items.removeClass("lightgrey");
    items.removeClass("small");
  }, 
  
  moreNizes: function() {
    this.trigger('more');
    this.nizes.getNext();
  },

  endNizes: function() {
    this.$el.find('.load-more').addClass('disabled');
  },

  newNizes: function() {
    this.$el.find('.load-more').removeClass('disabled');
  },

  openNize: function(nizeId) {
    var nizeOpenView = new NizeOpenView({nizeId: nizeId, collection: this.nizes});
  }

});
