// Base class for all views.

var BaseView = function (options) {

    this.bindings = [];
    Backbone.View.apply(this, [options]);
};

_.extend(BaseView.prototype, Backbone.View.prototype, {

  initialize: function() {
    this.render = _.bind(this.render, this);
  },

  template: function() {},
  getRenderData: function() {},

  render: function() {
    this.$el.html(this.template(this.getRenderData()));
    this.afterRender();
    return this;
  },

  afterRender: function() {},

  /**
   * Dispose View functions
   */

  bindTo: function(model, ev, callback) {
    model.bind(ev, callback, this);
    this.bindings.push({model:model, ev: ev, callback: callback});
  },

  unbindFromAll: function() {
    _.each(this.bindings, function (binding) {
      binding.model.unbind(binding.ev, binding.callback);
    });
    this.bindings = [];
  },

  dispose: function () {
    this.disposeViews();
    this.unbindFromAll(); // this will unbind all events that this view has bound to
    this.unbind(); // this will unbind all listeners to events from this view. This is probably not necessary because this view will be garbage collected.
    this.remove(); // uses the default Backbone.View.remove() method which removes this.el from the DOM and removes DOM events.
  },

  disposeViews: function() {
    if (this.views) {
      _(this.views).each(function(view) {
        return view.dispose();
      });
    }
    return this.views = [];
  },

  renderView: function(el, func, view) {
    $.fn[func].call(el, view.render().el);
    this.views || (this.views = []);
    return this.views.push(view);
  }

});

BaseView.extend = Backbone.View.extend;

module.exports = BaseView;
