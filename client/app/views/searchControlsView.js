var View = require('./view');
var application = require('application');
var ListsView = require('./search/listsView');
var RatingsView = require('./search/ratingsView');
var SearchView = require('./search/searchView');

module.exports = View.extend({

  initialize: function() {

    // add list dropdown control
    var listsView = new ListsView({ collection: this.options.listCollection });
    this.bindTo(listsView, 'selection', this.newListSelection);
    this.renderView(this.$el, 'append', listsView );

    // add rating dropdown control
    var ratingsView = new RatingsView();
    this.bindTo(ratingsView, 'selection', this.newRatingSelection);
    this.renderView(this.$el, 'append', ratingsView );

    // add search control
    var  searchView = new SearchView();
    this.bindTo(searchView, 'newSearch', this.newSearch);
    this.renderView(this.$el, 'append', searchView);

    // Fetch Nizes
    this.options.nizeCollection.search();
  },

  newListSelection: function(listId, listName) {
    this.options.nizeCollection.setListFilter(listId, listName);
  },
  
  newRatingSelection: function(ratingValue) {
    this.options.nizeCollection.setRatingFilter(ratingValue);
  },

  newSearch: function(key) {
    this.options.nizeCollection.setKeyFilter(key);
  },

  changeStyle: function(state) {
  }

});
