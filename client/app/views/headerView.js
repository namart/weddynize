var View = require('./view');
var application = require('application');

module.exports = View.extend({

  el: '#header',

  events: {
    "click a.nize-state": "changeNizeState", 
    "click .dropdown a.select-button": "toggleBox", 
  },

  initialize: function() {

    _.bindAll(this, 'smallerHeader', 'biggerHeader');

    this.buttons = [];

    // Save join Button
    var joinButton = this.$el.find('.join.button');
    joinButton.data('class', 'join');
    this.buttons.push(joinButton);

    // Save login Button
    var loginButton = this.$el.find('.login.button');
    loginButton.data('class', 'login');
    this.buttons.push(loginButton);

    var self = this;
    $(window).scroll(function(){
      if($(document).scrollTop() > 0 && !self.$el.hasClass("sticky")){
        self.fixHeader();
      } else {
        setTimeout(function(){
          if($(document).scrollTop() == 0 && self.$el.hasClass("sticky")) {
            self.unfixHeader();  
          }
        }, 1);
      }
    });
  },
  
  toggleBox: function(evt) {
    var self = this;
    var element = $(evt.target).closest(".dropdown");
    evt.stopPropagation();
    if(element.hasClass('open')) {
      this.closeBox(element);
    } else {
      element.addClass('open');
      $(document).bind('click', function(evt) {
        self.closeBox(element);
      });
    }
    return false;
  },

  closeBox: function(element) {
    
    element.removeClass('open');
  },

  fixHeader: function() {
    this.$el.addClass("sticky");
    this.$el.next().css({"margin-top": (this.$el.outerHeight(true) - 6)});
    this.smallerHeader();
    this.trigger('change', true);
  },

  unfixHeader: function() {
    this.$el.removeClass("sticky");
    this.$el.next().css({'margin-top': '0'});
    this.biggerHeader();
    this.trigger('change', false);
  },

  smallerHeader: function() {
    this.$el.addClass('smaller');
  },

  biggerHeader: function() {
    this.$el.removeClass('smaller');
  },

  changeNizeState: function(evt) {
    var url = Backbone.history.fragment;
    if(url === '' || url === 'all') {
      application.router.navigate($(evt.target).attr('href'), {trigger: true});
      return false;
    }
  }, 

});
