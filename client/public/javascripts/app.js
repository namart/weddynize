(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return require(absolute);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    definition(module.exports, localRequire(name), module);
    var exports = cache[name] = module.exports;
    return exports;
  };

  var require = function(name) {
    var path = expand(name, '.');

    if (has(cache, path)) return cache[path];
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex];
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '"');
  };

  var define = function(bundle) {
    for (var key in bundle) {
      if (has(bundle, key)) {
        modules[key] = bundle[key];
      }
    }
  }

  globals.require = require;
  globals.require.define = define;
  globals.require.brunch = true;
})();

window.require.define({"application": function(exports, require, module) {
  // Application bootstrapper.
  Application = {
    initialize: function() {

      // load in App data
      _.extend(this, App);

      var Router = require('lib/router');
      var ViewHelper = require('lib/view_helper');
      var HeaderView = require('views/headerView');
      var AddNizeView = require('views/nize/addNizeView');
      var NizeLayoutView = require('views/nizeLayoutView');

      this.headerView = new HeaderView();
      var nizeLayoutView = new NizeLayoutView();
      var addNizeView = new AddNizeView();

      this.router = new Router();

      if (typeof Object.freeze === 'function') Object.freeze(this);
    }
  };

  module.exports = Application;
  
}});

window.require.define({"initialize": function(exports, require, module) {
  var application = require('application');

  $(function() {

    var options = {
      lng: application.lang,
      useLocalStorage: false,
      resGetPath: '/locales/resources.json?lng=__lng__&ns=__ns__',
      dynamicLoad: true,
      sendMissing: false
    };

    //First of all, we need to load locales, after that we can do all the rest
    $.i18n.init(options, function() {
      application.initialize();
      Backbone.history.start({pushState: true});
    });


    //This is a little form helper to remove form-labels when user clicks in input field
    $("input.error").focus(function(){
      $(this).parent().find("label.errormessage").fadeOut(200);
    });

  });
  
}});

window.require.define({"lib/router": function(exports, require, module) {
  var application = require('application');

  module.exports = Backbone.Router.extend({

    routes: {
      '' : 'home',
      'all': 'homeAll',
      'nize/add' : 'addNize',
      'nize/:id' : 'nize',
      '_=_' : 'facebookLoginBug'
    },

    home: function() {
      Backbone.Mediator.publish('nizeLayoutView:render');
    },

    homeAll: function() {
      Backbone.Mediator.publish('nizeLayoutView:render', true);
    },

    addNize: function() {
      Backbone.Mediator.publish('addNize:init');
    },

    nize: function(nizeId) {
      Backbone.Mediator.publish('openNize:init', nizeId);
    },

    facebookLoginBug: function() {
      this.navigate("/", {trigger: true, replace: true});
    }

  });
  
}});

window.require.define({"lib/view_helper": function(exports, require, module) {
  // Put your handlebars.js helpers here.

  Handlebars.registerHelper('t',
    function(str){

      trans = $.t(str);
      if(trans != "") {
        str = trans;
      }

      return str;
    }
  );
}});

window.require.define({"models/collection": function(exports, require, module) {
  // Base class for all collections.
  module.exports = Backbone.Collection.extend({
    
  });
  
}});

window.require.define({"models/listCollection": function(exports, require, module) {
  var Collection = require('./collection');
  var ListModel = require('./listModel');

  module.exports = Collection.extend({

    url: '/list',

    model: ListModel,

    showByAttribute: function(attr) {
      var filteredLists = this.where(attr);
      this.trigger('filteredLists', filteredLists);
    }

  });
  
}});

window.require.define({"models/listModel": function(exports, require, module) {
  var Model = require('./model');

  module.exports = Model.extend({

    idAttribute: '_id',

    urlRoot: '/list',

    defaults: function() {
      return {
        open:  false,
        checked : false
      };
    }

  });
  
}});

window.require.define({"models/model": function(exports, require, module) {
  // Base class for all models.
  module.exports = Backbone.Model.extend({
    
  });
  
}});

window.require.define({"models/nizeCollection": function(exports, require, module) {
  var Collection = require('./collection');
  var NizeModel = require('./nizeModel');

  module.exports = Collection.extend({

    urlRoot: '/nize',

    model: NizeModel,

    initialize: function() {

      this.listFilter = '';
      this.listFilterName = '';
      this.ratingFilter = '';
      this.keyFilter = '';
      this.open = false;
      this.limit = 20;

      this.setSearchDefaults();
    },

    setSearchDefaults: function() {
      this.offset = 0;
      this.endReached = false;
    },

    setListFilter: function(listId, listName) {
      console.log(listName);
      this.setSearchDefaults();
      this.listFilter = listId;
      this.listFilterName = listName;
      this.search();
    },

    setRatingFilter: function(ratingValue) {
      this.setSearchDefaults();
      this.ratingFilter = ratingValue;
      this.search();
    },

    setKeyFilter: function(key) {
      this.setSearchDefaults();
      this.keyFilter = key;
      this.search();
    },

    setOpenFilter: function(state) {
      this.setSearchDefaults();
      this.open = state;
      this.search();
    },

    getNext: function() {
      this.offset += this.limit;
      this.search();
    },

    getFilters: function() {
      if(this.listFilter || this.ratingFilter || this.keyFilter) {
        return {
          list: this.listFilterName,
          rating: this.ratingFilter,
          key: this.keyFilter
        };
      } else {
        return null;
      }
    },

    search: function(data) {
      if(this.endReached) return;
      var url = this.urlRoot + '/search.json';
      var self = this;
      data = _.extend({
        key: this.keyFilter,
        list: this.listFilter,
        rating: this.ratingFilter,
        limit: this.limit,
        open: this.open,
        offset: this.offset
      },data);

      $.ajax({
        url:url,
        data: data,
        dataType:"json",
        success:function (data) {
          //console.log("search success: " + data.length, data);

          // append or reset
          if(self.offset === 0) {
            self.reset(data);
          } else {
            self.add(data);
          }

          // send event if all data already loaded
          if(data.length < self.limit) {
            self.endReached = true;
            self.trigger('finished');
          } else {
            self.trigger('newSearch');
          }
        }
      });
    }

  });
  
}});

window.require.define({"models/nizeModel": function(exports, require, module) {
  var Model = require('./model');
  var ListModel = require('./listModel');

  module.exports = Model.extend({

    idAttribute: '_id',
    urlRoot: '/nize', 
    
  });
  
}});

window.require.define({"views/headerView": function(exports, require, module) {
  var View = require('./view');
  var application = require('application');

  module.exports = View.extend({

    el: '#header',

    events: {
      "click a.nize-state": "changeNizeState", 
      "click .dropdown a.select-button": "toggleBox", 
    },

    initialize: function() {

      _.bindAll(this, 'smallerHeader', 'biggerHeader');

      this.buttons = [];

      // Save join Button
      var joinButton = this.$el.find('.join.button');
      joinButton.data('class', 'join');
      this.buttons.push(joinButton);

      // Save login Button
      var loginButton = this.$el.find('.login.button');
      loginButton.data('class', 'login');
      this.buttons.push(loginButton);

      var self = this;
      $(window).scroll(function(){
        if($(document).scrollTop() > 0 && !self.$el.hasClass("sticky")){
          self.fixHeader();
        } else {
          setTimeout(function(){
            if($(document).scrollTop() == 0 && self.$el.hasClass("sticky")) {
              self.unfixHeader();  
            }
          }, 1);
        }
      });
    },
    
    toggleBox: function(evt) {
      var self = this;
      var element = $(evt.target).closest(".dropdown");
      evt.stopPropagation();
      if(element.hasClass('open')) {
        this.closeBox(element);
      } else {
        element.addClass('open');
        $(document).bind('click', function(evt) {
          self.closeBox(element);
        });
      }
      return false;
    },

    closeBox: function(element) {
      
      element.removeClass('open');
    },

    fixHeader: function() {
      this.$el.addClass("sticky");
      this.$el.next().css({"margin-top": (this.$el.outerHeight(true) - 6)});
      this.smallerHeader();
      this.trigger('change', true);
    },

    unfixHeader: function() {
      this.$el.removeClass("sticky");
      this.$el.next().css({'margin-top': '0'});
      this.biggerHeader();
      this.trigger('change', false);
    },

    smallerHeader: function() {
      this.$el.addClass('smaller');
    },

    biggerHeader: function() {
      this.$el.removeClass('smaller');
    },

    changeNizeState: function(evt) {
      var url = Backbone.history.fragment;
      if(url === '' || url === 'all') {
        application.router.navigate($(evt.target).attr('href'), {trigger: true});
        return false;
      }
    }, 

  });
  
}});

window.require.define({"views/list/listUnitView": function(exports, require, module) {
  var View = require('../view');
  var template = require('../templates/list/listUnit');

  module.exports = View.extend({

    template: template,

    getRenderData: function() {
      return {
        list: this.model.toJSON()
      };
    }

  });
  
}});

window.require.define({"views/nize/addNizeView": function(exports, require, module) {
  var View = require('../view');
  var Nize = require('models/nizeModel');
  var List = require('models/listModel');
  var ListCollection = require('models/listCollection');
  var template = require('../templates/nize/addNizeForm');
  var ListUnitView = require('../list/listUnitView');
  var application = require('application');

  module.exports = View.extend({

    el: '.add-nize-right',

    template: template,

    initialize: function() {
      //console.debug('init#addNize', this.collection);

      _.bindAll(this, 'addOneList');

      this.c = document.createElement( 'canvas' );
      this.img = null;
      this.file = null;
      this.lists = null;

      // supported file types
      this.fileSupport = ['image/jpeg', 'image/png'];
    },

    events: {
      'change .fileselect' : 'fileSelectHandler',
      'dragover .filedrag' : 'fileDragHover',
      'dragleave .filedrag' : 'fileDragHover',
      'drop .filedrag' : 'fileSelectHandler',
      'keypress .nize-new-list' : 'newList',
      'click .nize-submit' : 'createNize'
    },

    subscriptions: {
      'addNize:init': 'init'
    },

    init: function() {

      // only if browser supports File API
      if (window.File && window.FileList && window.FileReader) {

        var xhr = new XMLHttpRequest();
        if(xhr.upload) {
          this.$el.find('.filedrag').css('display', 'block');
          this.$el.find('.submitbutton').css('display', 'none');
        }

      } else {
        this.undelegateEvents();
      }
    },

    fileDragHover: function(e) {
      e.stopPropagation();
      e.preventDefault();
      if(e.type == "dragover") {
        $(e.target).addClass("hover");
      } else {
        $(e.target).removeClass("hover");
      }
    },

    fileSelectHandler: function(e) {

      // cancel event and hover styling
      this.fileDragHover(e);

      // fetch FileList object
      var files = e.target.files || e.originalEvent.dataTransfer.files;

      var file = files[0];
      this.file = file;
      if ( file.type.indexOf( 'image' ) === 0 ) {
        this.showNizeForm();
      } else {
        alert('Only Images allowed');
      }

    },

    showNizeForm: function() {
      var url = window.URL || window.webkitURL,
          self = this,
          data = null,
          img = new Image(),
          reader = new FileReader(),
          objURL = url.createObjectURL || false;

      this.lists = new ListCollection(application.lists);
      this.bindTo(this.lists, 'add', this.addOneList);
      this.$el.find('.upload').css('display', 'none');
      this.$el.append(this.template( { lists: this.lists.toJSON() } ));

      // add lists
      _.each(this.lists.models, _.bind(function(list) {
        this.renderView(this.$el.find('.nize-lists'), 'append', new ListUnitView({ model: list }));
      }, this));

      // add rating
      _.each(application.ratings, _.bind(function(rating) {
        this.$el.find('.nize-rating select').append($('<option>', {value: rating.value, text: rating.text}))
      }, this));

      // show image
      if(objURL) {
        img.src = url.createObjectURL(this.file);
        this.$el.prepend(img);
      } else {
        reader.readAsDataURL( this.file );
        reader.onload = function ( ev ) {
          img.src = ev.target.result;
          img.onload = function() {
            self.$el.prepend(this);
          };
        };
      }

    },

    addOneList: function(list) {
      this.renderView(this.$el.find('.nize-lists'), 'append', new ListUnitView({ model: list }));
    },

    newList: function(evt) {
      if(evt.which == 13) {
        var listName = this.$el.find('.nize-new-list input').val();
        var self = this;
        var list = new List();
        list.save({ name: listName, checked: true }, {
          wait:true,
          success:function(model, response) {
            self.lists.add(model);
          },
          error: function(model, error) {
            console.log(model.toJSON());
            console.log('error.responseText');
          }
        });
      }
    },

    createNize: function() {
      var title = this.$el.find('.nize-title input').val();
      var rating = this.$el.find('.nize-rating select').val();
      var description = this.$el.find('.nize-description input').val();
      var self = this;
      var lists = $('.nize-lists input:checked');

      lists = $.map(lists, function(v, k) {
        return $(v).val();
      });

      var nizeObj = {
        title: title,
        description: description,
        lists: lists,
        rating: rating,
        link: ''
      };

      this.nize = new Nize();
      this.nize.save(nizeObj, {
        wait:true,
        success:function(model, response) {
          console.log('successfully added nize');
          self.uploadImage();
        },
        error: function(model, error) {
          console.log(model.toJSON());
          console.log('error.responseText');
        }
      });

    },

    uploadImage: function() {
      var xhr = new XMLHttpRequest();

      if (xhr.upload && this.fileSupport.indexOf(this.file.type) > -1 && this.file.size <= this.$el.find("#MAX_FILE_SIZE").val()) {

        // progress bar
        xhr.upload.addEventListener("progress", function(e) {
          var pc = parseInt(100 - (e.loaded / e.total * 100), 10);
          debug.log(pc + "%");
        }, false);

        // file received/failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            debug.log(xhr.status == 200 ? "success upload" : "upload failure");
          }
        };

        var formdata = new FormData();
        formdata.append(this.file.name, this.file);
        formdata.append('nizeId', this.nize.get('_id'));

        // start upload
        xhr.open("PUT", this.$el.find('.upload').get(0).action, true);
        xhr.setRequestHeader("X_FILENAME", this.file.name);
        xhr.send(formdata);

      }
    }

  });
  
}});

window.require.define({"views/nize/nizeUnitView": function(exports, require, module) {
  var View = require('../view');
  var template = require('../templates/nize/nizeUnit');
  var ratingTemplate = require('../templates/shared/rating');
  var application = require('application');

  module.exports = View.extend({

    className: 'nize',

    template: template,

    getRenderData: function() {
      return {
        nize: this.model.toJSON()
      };
    },

    events: {
      "click  a.nize-image": "openNize"
    },

    initialize: function() {    
      //console.log('order: ', this.options.order);
      var cssOptions = _.extend({opacity: 0}, this.options.css);
      this.$el.css(cssOptions);
      
      this.bindTo(this.model, 'change', this.updateView);

      // fade in depending on the order
      setTimeout(_.bind(function() {
        this.$el.css('opacity', 1);
      }, this), 50*this.options.index);
    },

    afterRender: function() {
      var perc = 100/4*this.model.get('rating');
      this.$el.append(ratingTemplate({
        ratings: application.ratings,
        rating: perc,
        current: this.model.get('rating'), 
        notActive: true, 
      }));
    },

    openNize: function(evt) {
      evt.preventDefault();
      application.router.navigate('nize/' + this.model.get('_id'), {trigger: true});
    }, 
    
    updateView: function(obj) {
      this.$el.find("*[role=title]").text(obj.get("title"));
      this.$el.find("*[role=description]").text(obj.get("description"));
      
      this.$el.find("ul.rating .current").text("" + obj.get("rating") + " out of 4");
      this.$el.find("ul.rating .current").width("" + obj.get("rating")*25 + "%");
    }, 

  });
  
}});

window.require.define({"views/nize/nizesView": function(exports, require, module) {
  var View = require('../view');
  var NizeUnitView = require('./nizeUnitView');
  var noNizeFoundView = require('../search/noNizeFoundView');
  var application = require('application');

  module.exports = View.extend({

    gridOptions: {
      itemWidth: 232,
      gutter: 24
    },

    initialize: function() {
      //console.debug('init#nizeView', this.collection);

      // Layout variables.
      this.columns = null;
      this.columnWidth = null;
      this.heights = [];

      //More Nize index holder
      this.nizeIndex = 0;

      //Bindings
      this.bindTo(this.collection, 'reset', this.addAll);
      this.bindTo(this.collection, 'add', this.addOne);
      this.bindTo(this.options.parent, 'more', function(){ this.nizeIndex = 0;});
      this.bindTo(this.collection, 'destroy', function(){
        this.addAll;
        this.addAll(this.collection);
      });
      
      //we need to add this margin, for the scrolling effect
      $("#header").css({"margin-bottom": "55px"});
      
      //window resize event
      var resizeTimer = null;
      $(window).resize(_.bind(function(event) {
        if(resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(
          _.bind(
            this.layout, 
            this, 
            this.$el.find('.nize').length < 7 ? 1020 : null
          ), 100
        );
      }, this));
    },

    layout: function(maxContainerWidth) {

      // Calculate basic layout parameters.
      var containerWidth = maxContainerWidth || $(window).outerWidth();
      this.columnWidth = this.gridOptions.itemWidth + this.gridOptions.gutter;
      
      var columns = Math.floor((containerWidth + this.gridOptions.gutter)/this.columnWidth);
      
      // minimal 4 columns
      if(columns < 4) columns = 4;
      
      var oldColumns = this.columns;
      this.columns = columns = columns%2 ? (columns-1<4 ? columns : columns-1) : columns;

      var newContainerWidth = Math.round(columns*this.columnWidth-this.gridOptions.gutter);
      this.$el.css('width', newContainerWidth);


      // Prepare Array to store height of columns.
      this.heights = [];
      while(this.heights.length < columns) {
        this.heights.push(0);
      }

      // resize items if exists
      if(this.$el.find('.nize').length && oldColumns !== columns) {
        this.disposeViews();
        this.collection.each(_.bind(this.addOne, this));
      }
    },

    addAll: function(nizes) {
      this.disposeViews();
      this.layout(nizes.length < 7 ? 1020 : null);
      if(nizes.length === 0) {
        this.renderView(this.$el, 'append', new noNizeFoundView({parent: this, filters: nizes.getFilters()}));
      } else {
        this.nizeIndex = 0;
        nizes.each(_.bind(function(nize, counter) {
          this.addOne(nize, nizes, {index: counter});
        }, this));
      }
    },

    addOne: function(nize, nizes, options) {

      var index = options.index;
      if(this.nizeIndex === 0) this.nizeIndex = index;
      options.index = index - this.nizeIndex;

      var self = this;

      // Find the shortest column.
      var shortest = null;
      var shortestIndex = 0;

      for(k=0; k<this.columns; k++) {
        if(shortest === null || this.heights[k] < shortest) {
          shortest = this.heights[k];
          shortestIndex = k;
        }
      }
      
      var cssChanges = {
        position: 'absolute',
        top: shortest,
        left: shortestIndex * self.columnWidth, 

      };

      var nizeUnitView = new NizeUnitView({
        model:nize,
        css: cssChanges,
        id: nize.get('_id'),
        index: options.index
      });
      this.renderView(this.$el, 'append', nizeUnitView);
        
      var colHeight = shortest + nizeUnitView.$el.outerHeight() + this.gridOptions.gutter;
      this.heights[shortestIndex] =  colHeight;
      this.$el.css('height', colHeight);
    }

  });
  
}});

window.require.define({"views/nize/open/nizeOpenUnitView": function(exports, require, module) {
  var View = require('../../view');
  var application = require('application');
  var template = require('../../templates/nize/open/nize');
  var ratingTemplate = require('../../templates/shared/rating');

  module.exports = View.extend({

    className: 'nize-open-unit nize',

    template: template,

    getRenderData: function() {
      return {
        nize: this.model.toJSON()
      };
    },

    events: {
      "click .nize-field-edit"  : "inlineEditBegin",
      "blur .nize-field-edit"   : "inlineEditEnd",
      "click a.delete"          : "deleteNize",
      "click ul.rating"         : "changeRating",
      "click"                   : "preventClosing",
      //"keydown .nize-field-edit" : "preventMultiline",
    },

    initialize: function() {
      //console.debug('init#nizeOpenUnit', this.collection);
      this.$el.css(this.options.css);

      if(this.canEditNize()) {
        this.model.set("editable", true);
      }
    },

    afterRender: function() {

      // Load big picture
      var $image = this.$el.find('img');
      var img = new Image();
      var self = this;
      $(img).bind('load error', function(e){
        //now its loaded so show it
        $image.css('visibility', 'visible');
        // setting css properties for nice opening animation
        $image.css('margin-left',-($image.width()-200)/2);
        $image.css('height', img.height);
        //notify as ready
        self.trigger('imgLoaded');
        //wait for animation
        setTimeout(function() {
          self.loadContent();
        }, 300);
      });
      img.src = $image.attr('src');
    },

    loadContent: function() {

      this.$el.parent().removeClass('loading').addClass('loaded');

      // show rating
      var perc = 100/4*this.model.get('rating');
      this.$el.find('.nize-open-header').append(ratingTemplate({
        ratings: application.ratings,
        rating: perc,
        notActive: !this.canEditNize(),
      }));

      if(this.canEditNize) {
        this.$el.find('.nize-field-edit').addClass('show-edit');
        this.$el.find('.nize-field-edit').attr("contenteditable", true);

        setTimeout(_.bind(function() {
          this.$el.find('.nize-field-edit').removeClass('show-edit');
        }, this), 1000);
      }
    },

    canEditNize: function() {
      if(application.user == this.model.get("_creator")) {
        return true;
      }

      return false;
    },

    preventClosing: function(evt) {
      evt.stopPropagation();
    },

    inlineEditBegin: function(evt) {
      if(this.canEditNize()) {
        //evt.target.setAttribute("contenteditable", true);
      }
    },

    inlineEditEnd: function(evt) {
      if(this.canEditNize()) {

        // disable content editable
        //evt.target.setAttribute("contenteditable", false);

        this.highlight(evt.target);

        // get element role and change attribute if valid
        role = evt.target.getAttribute("role");
        if(this.model.has(role)) {
          this.model.save(role, evt.target.innerHTML, {error: this.errorOnSave});
        }
      }
    },

    changeRating: function(evt) {
      if(this.canEditNize()) {
        if(evt.target.hasAttribute("data-rating-value")) {
          this.model.save("rating", evt.target.getAttribute("data-rating-value"), {error: this.errorOnSave});
          this.$el.find("ul.rating").removeClass("no-active");
          this.$el.find("ul.rating .current").text("" + this.model.get("rating") + " out of 4");
          this.$el.find("ul.rating .current").width("" + this.model.get("rating")*25 + "%");
        }
      }
    },

    errorOnSave: function(model, response) {
      alert("ERROR WHILE SAVING!");
    },

    preventMultiline: function(evt) {
      if(evt.keyCode == 13) {
        evt.stopPropagation();
        return false;
      }
    },

    deleteNize: function(evt) {

      if (confirm('Are you sure you want to delete this nize?')) {
        this.model.destroy({wait: true});
      }
    },

    highlight: function(obj, error){
      $(obj).addClass("highlight");

      if(error) {
        $(obj).addClass("error");
      }

      setTimeout(function(){
        $(obj).removeClass("highlight");
        $(obj).removeClass("error");
      }, 200);
    }

  });
  
}});

window.require.define({"views/nize/open/nizeOpenView": function(exports, require, module) {
  var View = require('../../view');
  var NizeOpenUnitView = require('./nizeOpenUnitView');
  var application = require('application');

  module.exports = View.extend({

    className: 'nize-open',

    events: {
      "click": "close"
    },

    initialize: function() {
      //console.debug('init#nizeOpen', this.collection);
      
      this.bindTo(this.collection, 'destroy', this.close);
      
      var $nizeElement = $('#'+this.options.nizeId);
      var startPosition = $nizeElement.offset();
      var self = this;

      // calculate top for scrolled states
      startPosition.top = startPosition.top - $(document).scrollTop();

      // Set page to the background
      this.scrollBarWidth = this.getScrollBarWidth();
      $('body').addClass('noscroll');
      $('body').css('margin-right', this.scrollBarWidth);
      $('.l-search-controls-inner').css('width', $('.l-search-controls-inner').width() + this.scrollBarWidth);

      // add open nize
      this.nizeOpenUnitView = new NizeOpenUnitView({css: startPosition, model: this.collection.get(this.options.nizeId)});
      this.bindTo(this.nizeOpenUnitView, 'imgLoaded', this.open);
      this.renderView(this.$el, 'append', this.nizeOpenUnitView);

      $('body').append(this.$el);

      //start loading state
      this.$el.addClass('loading');

      },

    open: function() {

      // start visible state
      this.$el.addClass('visible');

      // center nize
      var viewportWidth = $(document).outerWidth();
      this.nizeOpenUnitView.$el.css('left', viewportWidth/2 - 326 - (this.scrollBarWidth/2));

      // set css properties for nize opening animation
      this.nizeOpenUnitView.$el.find('img').css('margin-left', 0);

    },

    close: function(evt) {
      // stop loading nize when hin as target
      
      if(typeof(evt.preventDefault) == "function") {   
        evt.preventDefault();
      }

      // Bring Backgroun back to front
      $('body').removeClass('noscroll');
      $('body').css('margin-right', 0);
      $('.l-search-controls-inner').css('width', $('.l-search-controls-inner').width() - this.scrollBarWidth);

      application.router.navigate('/');
      this.dispose();
    },

    //Helper
    getScrollBarWidth: function() {
      var inner = document.createElement('p');
      inner.style.width = "100%";
      inner.style.height = "200px";

      var outer = document.createElement('div');
      outer.style.position = "absolute";
      outer.style.top = "0px";
      outer.style.left = "0px";
      outer.style.visibility = "hidden";
      outer.style.width = "200px";
      outer.style.height = "150px";
      outer.style.overflow = "hidden";
      outer.appendChild (inner);

      document.body.appendChild (outer);
      var w1 = inner.offsetWidth;
      outer.style.overflow = 'scroll';
      var w2 = inner.offsetWidth;
      if (w1 == w2) w2 = outer.clientWidth;

      document.body.removeChild (outer);

      return (w1 - w2);
    }

  });
  
}});

window.require.define({"views/nizeLayoutView": function(exports, require, module) {
  var View = require('./view');
  var application = require('../application');
  var NizeCollection = require('../models/nizeCollection');
  var ListCollection = require('../models/listCollection');
  var NizesView = require('./nize/nizesView');
  var NizeOpenView = require('./nize/open/nizeOpenView');
  var searchControlsView = require('./searchControlsView');

  module.exports = View.extend({

    el: '.nize-layout',

    events: {
      "click .load-more": "moreNizes"
    },

    subscriptions: {
      'nizeLayoutView:render': 'load',
      'openNize:init': 'openNize'
    },

    load: function(open) {

      // set open state depending if the user is logged in
      this.open = application.user ? false : true;

      // If url points to /all then show open nizes
      if(open) this.open = true;
      
      if (this.open) {
        this.manageMenu("/all");
      } else {
        this.manageMenu("/");
      }

      this.nizes || (this.nizes = new NizeCollection());
      this.lists || (this.lists = new ListCollection(application.lists));

      // bindings
      this.bindTo(this.nizes, 'finished', this.endNizes);
      this.bindTo(this.nizes, 'newSearch', this.newNizes);

      // sub views
      this.nizesView || (this.nizesView = new NizesView({ 
        el: this.$('.l-nizes'),
        collection: this.nizes,
        parent: this
      }));

      this.searchControlsView || (this.searchControlsView = new searchControlsView({ 
        el: $('.l-search-controls-inner'),
        listCollection: this.lists,
        nizeCollection: this.nizes
      }));

      // Set filter for the right nizes
      this.nizes.setOpenFilter(this.open);
      // start fetching lists
      this.lists.showByAttribute({open: this.open});

    },
    
    manageMenu: function(url) {
      
      //reset all menu items
      var items = $("nav.top-menu.filter ul li a");
      items.addClass("small");
      items.addClass("lightgrey");
      items.removeClass("blue");
      
      //mark current element
      var items = $('nav.top-menu.filter ul li a[href="' + url + '"]');
      items.removeClass("lightgrey");
      items.removeClass("small");
    }, 
    
    moreNizes: function() {
      this.trigger('more');
      this.nizes.getNext();
    },

    endNizes: function() {
      this.$el.find('.load-more').addClass('disabled');
    },

    newNizes: function() {
      this.$el.find('.load-more').removeClass('disabled');
    },

    openNize: function(nizeId) {
      var nizeOpenView = new NizeOpenView({nizeId: nizeId, collection: this.nizes});
    }

  });
  
}});

window.require.define({"views/search/listUnitView": function(exports, require, module) {
  var View = require('../view');

  module.exports = View.extend({

    tagName: 'li',

    className: 'text-type',

    events: {
      "click": "selected"
    },

    attributes: function() {
      return {
        'data-list-id': this.model.get('_id') ? this.model.get('_id') : 0
      }
    },

    render: function() {
      $(this.el).html(this.model.get('name'));
      this.afterRender();
      return this;
    },

    selected: function() {
      this.trigger('selection', this);
    }

  });
  
}});

window.require.define({"views/search/listsView": function(exports, require, module) {
  var View = require('../view');
  var List = require('/models/listModel');
  var ListUnitView = require('./listUnitView');
  var template = require('../templates/search/dropdown');

  module.exports = View.extend({

    className: 'dropdown',

    template: template,

    getRenderData: function() {
      return {
        title: "Category:",
        defaultValue: "All"
      };
    },

    events: {
      "click a.current": "toggleBox"
    },

    initialize: function() {
      _.bindAll(this, 'addOne', 'addAll', 'selected');
      this.bindTo(this.collection, 'filteredLists', this.addAll);
    },

    addAll: function(lists) {
      this.disposeViews();
      this.addOne(new List({_id: '0', name: 'all'}));
      _.each(lists, this.addOne);
    },

    addOne: function(list) {
      var listUnitView = new ListUnitView({ model: list });
      this.bindTo(listUnitView, 'selection', this.selected);
      this.renderView(this.$el.find('ul'), 'append', listUnitView);
    },

    toggleBox: function(evt) {
      var self = this;
      evt.stopPropagation();
      if(this.$el.hasClass('open')) {
        this.closeBox();
      } else {
        this.$el.addClass('open');
        $(document).bind('click', function(evt) {
          self.closeBox();
        });
      }
      return false;
    },

    closeBox: function() {
      this.$el.removeClass('open');
    },

    selected: function(list) {
      this.$el.find('.current').html(list.$el.html());
      this.trigger('selection', list.$el.data('list-id'), list.$el.text());
    }

  });
  
}});

window.require.define({"views/search/noNizeFoundView": function(exports, require, module) {
  var View = require('../view');
  var template = require('../templates/nize/noNize');

  module.exports = View.extend({

    className: 'no-nize-found',

    template: template,

    getRenderData: function() {
      return {
        filters: this.options.filters
      };
    },

    afterRender: function() {
      this.options.parent.$el.css('height', 'auto');
    }

  });
  
}});

window.require.define({"views/search/ratingsView": function(exports, require, module) {
  var View = require('../view');
  var application = require('application');
  var ratingTemplate = require('../templates/shared/rating');

  module.exports = View.extend({

    tagName: 'div',

    className: 'ratingfilter',

    events: {
      "click": "removeRate",
      "click .rating": "selection"
    },

    initialize: function() {
      this.$el.addClass("bordered");
    },

    afterRender: function() {
      var description = $('<label>', {text:'Rating: '});
      this.$el.append(description);
      this.$el.append(ratingTemplate({ratings: application.ratings, rating: 0}));
    },

    removeRate: function() {
      this.setRate(0);
      this.trigger('selection', 0);
      return false;
    },

    selection: function(evt) {
      evt.stopPropagation();
      var rate = $(evt.target).text();
      this.setRate(rate);
      this.trigger('selection', rate);
      return false;
    },

    setRate: function(rate) {
      var perc = 100/4*rate;
      this.$el.find('.current').css('width', perc+'%');
    }

  });
  
}});

window.require.define({"views/search/searchView": function(exports, require, module) {
  var View = require('../view');
  var template = require('../templates/search/searchForm');

  module.exports = View.extend({

    className: 'search',

    template: template,

    events: {
      'keyup': 'newSearch'
    },

    initialize: function() {

      this.counter = 10;
      this.searched = false;

    },

    newSearch: function() {
      this.counter = 10;
      var self = this;
      var time = 30;
      var timerCallback = function() {
        self.counter--;
        if(self.counter === 0) {
          self.searched = false;
          self.trigger('newSearch', self.$el.find('input').val());
        } else {
          setTimeout(timerCallback, time);
        }
      };
      if(this.searched === false) {
        this.searched = true;
        setTimeout(timerCallback, time);
      }
    }

  });
  
}});

window.require.define({"views/searchControlsView": function(exports, require, module) {
  var View = require('./view');
  var application = require('application');
  var ListsView = require('./search/listsView');
  var RatingsView = require('./search/ratingsView');
  var SearchView = require('./search/searchView');

  module.exports = View.extend({

    initialize: function() {

      // add list dropdown control
      var listsView = new ListsView({ collection: this.options.listCollection });
      this.bindTo(listsView, 'selection', this.newListSelection);
      this.renderView(this.$el, 'append', listsView );

      // add rating dropdown control
      var ratingsView = new RatingsView();
      this.bindTo(ratingsView, 'selection', this.newRatingSelection);
      this.renderView(this.$el, 'append', ratingsView );

      // add search control
      var  searchView = new SearchView();
      this.bindTo(searchView, 'newSearch', this.newSearch);
      this.renderView(this.$el, 'append', searchView);

      // Fetch Nizes
      this.options.nizeCollection.search();
    },

    newListSelection: function(listId, listName) {
      this.options.nizeCollection.setListFilter(listId, listName);
    },
    
    newRatingSelection: function(ratingValue) {
      this.options.nizeCollection.setRatingFilter(ratingValue);
    },

    newSearch: function(key) {
      this.options.nizeCollection.setKeyFilter(key);
    },

    changeStyle: function(state) {
    }

  });
  
}});

window.require.define({"views/templates/layouts/nize": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


    buffer += "<div class=\"nizeControls\">\n</div>\n<div class=\"nizes\">\n</div>\n<p class=\"grid_1\">\n  <a href=\"#\" class=\"medium button\">";
    stack1 = "button.load-more";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    buffer += escapeExpression(stack1) + "</a>\n</p>";
    return buffer;});
}});

window.require.define({"views/templates/list/listUnit": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n      <input name=\"";
    foundHelper = helpers.list;
    stack1 = foundHelper || depth0.list;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1._id);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "list._id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" value=\"";
    foundHelper = helpers.list;
    stack1 = foundHelper || depth0.list;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1._id);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "list._id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" type=\"checkbox\" checked=\"checked\">\n    ";
    return buffer;}

  function program3(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n      <input name=\"";
    foundHelper = helpers.list;
    stack1 = foundHelper || depth0.list;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1._id);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "list._id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" value=\"";
    foundHelper = helpers.list;
    stack1 = foundHelper || depth0.list;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1._id);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "list._id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" type=\"checkbox\">\n    ";
    return buffer;}

    buffer += "<div class=\"list-item\" data-id=\"";
    foundHelper = helpers.list;
    stack1 = foundHelper || depth0.list;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1._id);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "list._id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">\n  <label for=\"";
    foundHelper = helpers.list;
    stack1 = foundHelper || depth0.list;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1._id);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "list._id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">";
    foundHelper = helpers.list;
    stack1 = foundHelper || depth0.list;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.name);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "list.name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\n    ";
    foundHelper = helpers.list;
    stack1 = foundHelper || depth0.list;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.checked);
    stack2 = helpers['if'];
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.program(3, program3, data);
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n  </label>\n</li>\n";
    return buffer;});
}});

window.require.define({"views/templates/nize/addNizeForm": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<ul>\n  <li class=\"nize-field\">\n    <label for=\"nize-title\">Title</label>\n    <div class=\"nize-title\">\n      <input name=\"nize-title\" type=\"text\">\n    </div>\n  </li>\n  <li class=\"nize-field\">\n    <label for=\"nize-description\">Description</label>\n    <div class=\"nize-description\">\n      <input name=\"nize-description\" type=\"text\">\n    </div>\n  </li>\n  <li class=\"nize-field\">\n    <label for=\"nize-lists\">lists</label>\n    <div class=\"nize-lists\"></div>\n  </li>\n  <li class=\"nize-field\">\n    <label for=\"nize-new-list\">new List</label>\n    <div class=\"nize-new-list\">\n      <input name=\"nize-new-list\" type=\"text\">\n    </div>\n  </li>\n  <li class=\"rating-field\">\n    <label for=\"rating-field\">Rating</label>\n    <div class=\"nize-rating\">\n      <select name=\"rating\">\n        <option value=\"0\" selected=\"selected\">Choose a rating</option>\n      </select>\n    </div>\n  </li>\n  <li class=\"nize-field\">\n    <input class=\"nize-submit\" type=\"submit\" value=\"create Nize\">\n  </li>\n</ul>\n";});
}});

window.require.define({"views/templates/nize/nizeUnit": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


    buffer += "<a href=\"/nize/";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1._id);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "nize._id", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" class=\"nize-image image-box\"><img src=\"";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.thumb);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "nize.thumb", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" class=\"thumb\"></a>\n<h2 role=\"title\">";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.title);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "nize.title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</h2>\n<p class=\"description\" role=\"description\">\n  ";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.description);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "nize.description", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\n</p>";
    return buffer;});
}});

window.require.define({"views/templates/nize/noNize": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n<div class=\"container_12\">\n  <h2>";
    stack1 = "headline.no-nize-in-category";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    buffer += escapeExpression(stack1) + "</h2>\n  <ol class=\"grid_4 push_4\">\n    <li>";
    stack1 = "paragraph.add-bookmarklet";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "</li>\n    <li>";
    stack1 = "paragraph.upload-nize";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "</li>\n  </ol>\n</div>\n";
    return buffer;}

  function program3(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n<div class=\"container_12\">\n  <h2>";
    stack1 = "headline.no-nize";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    buffer += escapeExpression(stack1) + "</h2>\n  <ol class=\"grid_4 push_4\">\n    <li>";
    stack1 = "paragraph.add-bookmarklet";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "</li>\n    <li>";
    stack1 = "paragraph.upload-nize";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "</li>\n  </ol>\n</div>\n";
    return buffer;}

    foundHelper = helpers.filters;
    stack1 = foundHelper || depth0.filters;
    stack2 = helpers['if'];
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.program(3, program3, data);
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n";
    return buffer;});
}});

window.require.define({"views/templates/nize/open/nize": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    
    return "nize-field-edit";}

  function program3(depth0,data) {
    
    
    return "nize-field-edit";}

  function program5(depth0,data) {
    
    var buffer = "", stack1, stack2;
    buffer += "\n	<p><a class=\"button pink delete\" href=\"javascript:void(0);\">";
    stack1 = "button.delete-nize";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    buffer += escapeExpression(stack1) + "</a></p>\n";
    return buffer;}

    buffer += "<div class=\"nize-open-header\">\n  <h2 class=\"";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.editable);
    stack2 = helpers['if'];
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\" role=\"title\">";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.title);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "nize.title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</h2>\n</div>\n<a href=\"";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.link);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "nize.link", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" class=\"image-box\">\n  <img src=\"";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.original);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "nize.original", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">\n</a>\n<p class=\"";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.editable);
    stack2 = helpers['if'];
    tmp1 = self.program(3, program3, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\" role=\"description\">";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.description);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "nize.description", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</p>\n\n";
    foundHelper = helpers.nize;
    stack1 = foundHelper || depth0.nize;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.editable);
    stack2 = helpers['if'];
    tmp1 = self.program(5, program5, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    return buffer;});
}});

window.require.define({"views/templates/search/dropdown": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


    buffer += "<a href=\"#\" class=\"current select-button bordered\"><span>";
    foundHelper = helpers.title;
    stack1 = foundHelper || depth0.title;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</span> ";
    foundHelper = helpers.defaultValue;
    stack1 = foundHelper || depth0.defaultValue;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "defaultValue", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</a>\n<ul class=\"dropdown-box\"></ul>\n";
    return buffer;});
}});

window.require.define({"views/templates/search/searchForm": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


    buffer += "<input type=\"text\" placeholder=\"";
    stack1 = "form.placeholder.search";
    foundHelper = helpers['t'];
    stack2 = foundHelper || depth0['t'];
    if(typeof stack2 === functionType) { stack1 = stack2.call(depth0, stack1, { hash: {} }); }
    else if(stack2=== undef) { stack1 = helperMissing.call(depth0, "t", stack1, { hash: {} }); }
    else { stack1 = stack2; }
    buffer += escapeExpression(stack1) + "\">";
    return buffer;});
}});

window.require.define({"views/templates/shared/rating": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

  function program1(depth0,data) {
    
    
    return "no-active";}

  function program3(depth0,data) {
    
    var buffer = "", stack1;
    buffer += "\n  <li><a href=\"javascript:void(0);\" class=\"rate";
    stack1 = depth0.value;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this.value", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" data-rating-value=\"";
    stack1 = depth0.value;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this.value", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\" title=\"";
    stack1 = depth0.text;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this.text", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">";
    stack1 = depth0.value;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "this.value", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</a></li>\n  ";
    return buffer;}

    buffer += "<ul class=\"rating ";
    foundHelper = helpers.notActive;
    stack1 = foundHelper || depth0.notActive;
    stack2 = helpers['if'];
    tmp1 = self.program(1, program1, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\">\n  <li class=\"current\" style=\"width: ";
    foundHelper = helpers.rating;
    stack1 = foundHelper || depth0.rating;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "rating", { hash: {} }); }
    buffer += escapeExpression(stack1) + "%;\">";
    foundHelper = helpers.current;
    stack1 = foundHelper || depth0.current;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "current", { hash: {} }); }
    buffer += escapeExpression(stack1) + " out of 5</li>\n  ";
    foundHelper = helpers.ratings;
    stack1 = foundHelper || depth0.ratings;
    stack2 = helpers.each;
    tmp1 = self.program(3, program3, data);
    tmp1.hash = {};
    tmp1.fn = tmp1;
    tmp1.inverse = self.noop;
    stack1 = stack2.call(depth0, stack1, tmp1);
    if(stack1 || stack1 === 0) { buffer += stack1; }
    buffer += "\n</ul>\n";
    return buffer;});
}});

window.require.define({"views/view": function(exports, require, module) {
  // Base class for all views.

  var BaseView = function (options) {

      this.bindings = [];
      Backbone.View.apply(this, [options]);
  };

  _.extend(BaseView.prototype, Backbone.View.prototype, {

    initialize: function() {
      this.render = _.bind(this.render, this);
    },

    template: function() {},
    getRenderData: function() {},

    render: function() {
      this.$el.html(this.template(this.getRenderData()));
      this.afterRender();
      return this;
    },

    afterRender: function() {},

    /**
     * Dispose View functions
     */

    bindTo: function(model, ev, callback) {
      model.bind(ev, callback, this);
      this.bindings.push({model:model, ev: ev, callback: callback});
    },

    unbindFromAll: function() {
      _.each(this.bindings, function (binding) {
        binding.model.unbind(binding.ev, binding.callback);
      });
      this.bindings = [];
    },

    dispose: function () {
      this.disposeViews();
      this.unbindFromAll(); // this will unbind all events that this view has bound to
      this.unbind(); // this will unbind all listeners to events from this view. This is probably not necessary because this view will be garbage collected.
      this.remove(); // uses the default Backbone.View.remove() method which removes this.el from the DOM and removes DOM events.
    },

    disposeViews: function() {
      if (this.views) {
        _(this.views).each(function(view) {
          return view.dispose();
        });
      }
      return this.views = [];
    },

    renderView: function(el, func, view) {
      $.fn[func].call(el, view.render().el);
      this.views || (this.views = []);
      return this.views.push(view);
    }

  });

  BaseView.extend = Backbone.View.extend;

  module.exports = BaseView;
  
}});

